![Olza Logistic Logo](src/img/olza-logo.png)

# CHANGELOG #
* unreleased changes going here:


---
* 1.8.11 (2024-01-13)
  * Improved error handling of config requesting invalid/disabled speditions
  * Translated remaining hardcoded error messages

* 1.8.10 (2024-12-04)
  * Logos pre-loaded from the server

* 1.8.9 (2024-11-28)
  * Fixed autosearch scrollbar

* 1.8.8 (2024-11-27)
  * FAN logo added

* 1.8.7 (2024-10-29)
  * Added version to the config call, fixing the sped. codes case

* 1.8.6 (2024-10-22)
  * Normalized spedition codes during initialization phase

* 1.8.5 (2024-10-09)
  * HRP logo added

* 1.8.4 (2024-07-29)
  * CP logo updated

* 1.8.3 (2024-05-17)
  * Added Ukrainian and Russian language versions
  * Added Kiev localization fallback for UA speditions

* 1.8.2 (2024-03-18)
  * Engine requirements adjusted

* 1.8.1 (2024-03-14)
  * Added Box Now (lockers) assets

* 1.8.0 (2023-08-11)
  * Widget no longer uses <A> tag to make it work with templates using <BASE> 

* 1.7.3 (2023-07-28)
  * Added Fan Courier (lockers) assets

* 1.7.2 (2023-07-04)
  * Code cleanup
  * Added Poczta Polska assets
  * Added Fan Courier assets

* 1.7.1 (2023-06-20)
  * Readme fixes

* 1.7.0 (2023-04-26)
  * Added support filter for "online" payment type.

* 1.6.2 (2023-02-24)
  * Cargus logos

* 1.6.1 (2023-02-16)
  * Added translations

* 1.6.0 (2023-02-06)
  * Added support for PP Type filter
  * Replaced external country flags with local SVG from https://github.com/lipis/flag-icons

* 1.5.6 (2023-01-16)
    * Added WEDO-BOX logo and pin image.

* 1.5.5 (2022-12-14)
    * Updated translations.

* 1.5.4 (2022-12-06)
    * Last clicked map location is now marked with a pin. 
    * Improved "Nearby points not found" message to be more descriptive and include radius distance.

* 1.5.3 (2022-11-30)
    * Spedition filter change now clears search input for better results.
    * Switching spedition filter no longer resets map zoom.
    * Changing spedition correctly lists nearby pickup points.
    * Updated developer docs.

* 1.5.2 (2022-11-25)
     * Widget will now properly detect faulty/unsupported language and try to fall back
       to locate based on user browser settings or eventually hardcoded default.

* 1.5.1 (2022-11-24)
     * Serialization of async PP load

 * 1.5.0 (2022-11-22)
     * Initial map location will now fallback to requested country's capital city.
     * List of pickup points fetched at startup is now limited to visible map boundary.

 * 1.4.4 (2022-11-22)
     * UI polishing. 

 * 1.4.3 (2022-10-28)
     * Corrected handling of initial map zoom that was not adjusted to fit all items. 

 * 1.4.2 (2022-10-18)
     * Fixed nearby list remaining empty when the list was filtered down to zero elements.
     * Corrected map and nearby list refresh on search filter switches change.


 * 1.4.1 (2022-10-14)
     * Fixed spedition load structure.


 * 1.4.0 (2022-10-13)
     * [BREAKING] Structure of configuration object changed. See [docs/config.md] for more.
     * [BREAKING] Names of `data-*` attributes changed. See [docs/config.md#attributes] for more.
     * Introduced filters preset and visibility control.
     * Config: `zoom` option is renamed to `initialZoom`.
     * Config: `showOnMapZoomLevel` option is renamed to `detailsZoom`.
     * Added more checks while injecting widget into page DOM.
     * Turned language selector into a dropdown menu.
     * Search Box is now visually "locked" if search query is in progress.
     * Posts error toast if cross-section of requested and provided speditions is empty.
     * Corrected user config and `data-*` driven params merge causing onSelect callback failures.
     * Fixed "Search" button occasionally triggering parent page reload.
     * Changed SPS-PP assets to use proper SPS-PS spedition code.
     * Added support for service/payment type search filters.
     * Nearby list will now also show Pickup Point dedicated name if there's such known.
     * Nearby list will show also Pickup Point feature related icons (i.e. CoD, Card payments etc).
     * Added option to control filters (i.e. CoD, Card payments etc) visibility and initial values.
     * Reworked search to use typeahead for better results.
     * `data-access-token` renamed to `data-olza-api-access-token`
     * `data-country` renamed to `data-olza-api-country`
     * `data-details-zoom` renamed to `data-olza-ui-details-zoom`
     * `data-initial-zoom` renamed to `data-olza-ui-initial-zoom`
     * `data-language` renamed to `data-olza-ui-language`
     * `data-latitude` renamed to `data-olza-api-latitude`
     * `data-longitude` renamed to `data-olza-api-longitude`
     * `data-speditions` renamed to `data-olza-api-speditions`
     * `data-url` renamed to `data-olza-api-url`
     * `data-use-browser-location` renamed to `data-olza-ui-use-browser-location`


 * 1.3.2 (2022-08-22)
     * Added new translations.


 * 1.3.1 (2022-08-03)
     * Fixed detail view pane CSS.


 * 1.2.8 (2022-07-18)
     * Widget configuration object is now logged if debug mode is on.
     * API URL string trailing slashes are now removed prior using the URL.


 * 1.1.6 (2022-05-10)
     * Added ZAS-COL graphic assets.


 * 1.1.5 (2022-04-21)
     * Added HUP-PM graphic assets.


 * 1.1.4 (2022-04-15)
     * Package size optimizations.


 * 1.1.3 (2022-04-07)
     * Corrected details pane closing callback.


 * 1.1.2 (2022-04-07)
     * Fixed Bootrstrap 5 conflicts.


 * 1.1.1 (2022-04-06)
     * Upated dependencies.
     * Improved documentations.
     * Opening hours section headers is visible on demand.
     * Moved error message to translation file.


 * 1.1.0 (2022-04-04)
     * Switched to use Bootstrap 5.
     * Updated Olza Logistic logo.
     * Introduced SVG UI icons.
     * Updated usage examples.


 * 1.0.3 (2022-03-22)
     * Nearby list is now updated along with PP search.
     * Added Venipak's (VENIP) pin and logo.
     * Switching carrier triggers map update as well.


 * 1.0.2 (2022-03-03)
     * Improved handling of widget's language change.
     * Added SAEB-BOX graphic assets.
     * Updated translations.
     * Error toast timeout increased to 20 seconds.
     * Changed Czech language code to `cs`.


 * 1.0.0 (2022-02-16)
     * Initial public release.
