![Olza Logistic Logo](../src/img/olza-logo.png)

# pp-api-widget #

Olza Logistic's Pickup Point selection widget.

---

## Configuration object

You can configure widget solely from code, by passing configuration object to `Olza.Widget.pick();`
or `Olza.Widget.embed();` methods or in hybrid way, by storing all your configuration data
in HTML markup, using `data-*` attributes. In the latter case, you still must create config
object to declare `onSelect` callback, but other than that, all configuration elements have their
`data-*` attributes counterparts.

### Config merging order ###

When using hybrid approach, nothing prevents you from having the same parameter configured
both in config object and `data-*` attributes. In fact this may come very handy for multiple
usage scenarios. To support that, Olza Widget does multi-stage config merge to build final
configuration for the widget. There're two merging modes:

* `data-* first`, when `data-*` attributes take precedence and are merged into config object
  provided via code, then constructed transient eventually merged into widget default config to
  produce final runtime configuration.
* `Code First`, when code provided configuration is merged into `data-*` attributes, next produced
  transient config is then merged into widget defaults to produce final runtime configuration.

The default mode is `data-* first`. To switch to Code First approach instead, add `codeFirst: true`
to root of your configuration object.

### Configuration structure

[Widget configuration objetc](../src/config.js) is organized as hierarchical structure, with
elements grouped together for easier control and maintenance. Some of the options can only be
passed from code (via configuration object) but other can also be configured by using `data-*`
attributes on HTML button or other clickable element.

### Default values

To see all the defaults, please see the [default configuration sources](../src/config.js).

#### API access

API access credentials and mandatory settings

| Parameter              | `data-*` attribute         | Description                                                                   | Notes                                                                                                                                               |
|------------------------|----------------------------|-------------------------------------------------------------------------------|-----------------------------------------------------------------------------------------------------------------------------------------------------|
| api.accessToken        | data-olza-api-access-token | OlzaLogistic's API access token.                                              |                                                                                                                                                     |
| api.country            | data-olza-api-country      | Target country to use library for.                                            |                                                                                                                                                     |
| api.latitude           | data-olza-api-latitude     | Latitude of the initial map location.                                         |                                                                                                                                                     |
| api.longitude          | data-olza-api-longitude    | Longitude of the initial map location.                                        |                                                                                                                                                     |
| api.speditions         | data-olza-api-speditions   | List of spedition codes you want to be used.                                  | Pass empty `[]` to use all speditions API supports. If using `data-*` attributes, multiple speditions must be passed as comma separated **string**. |
| api.url                | data-olza-api-url          | OlzaLogistic's API base URL.                                                  |                                                                                                                                                     |
| api.useBrowserLocation |                            | If `true`, will try to use the browser's geolocation as map initial location. |                                                                                                                                                     |

#### UI elements

You can control appearance of the widget and visibility or behavior of certain UI elements by
setting available UI related settings

| Parameter                     | `data-*` attribute                | Description                                                                      | Notes                                                          |
|-------------------------------|-----------------------------------|----------------------------------------------------------------------------------|----------------------------------------------------------------|
| ui.detailsZoom                | data-olza-ui-details-zoom         | Zoom level to show selected Pickup Point on the map.                             |                                                                |
| ui.divId                      |                                   | ID of the root container the widget will be embedded into.                       |                                                                |
| ui.initialZoom                | data-olza-ui-initial-zoom         | Initial Leaflet map zoom level.                                                  |                                                                |
| ui.language                   | data-olza-ui-language             | Requested UI language.                                                           | Will use browser defined language if not explicitly specified. |
| ui.paymentTypeFilters.visible |                                   | Visibility of payment types filter's UI elements.                                |                                                                |
| ui.serviceTypeFilters.visible |                                   | Visibility of available service (CoD) filter's UI elements.                      |                                                                |
| ui.useBrowserLocation         | data-olza-ui-use-browser-location | If `true`, will try to use browser's location as user starting point on the map. | For data attributes, use `true` or `false` strings as value.   |

#### Data filtering

We heard some of users need to hard-fix specific data filtering schema and make it non-alterable.
We listened and now you can control both default state of the specific switch (`false` switch is
off, `true` switch is checked) as well as visibility of corresponding UI elements. That allows you
to i.e. enforce only PPs with `CoD` explicitly announced (by setting initial filter flag to `true`
and then prevent user from changing that by disabling (hiding) filter UI elements.

| Parameter                    | `data-*` attribute | Description                                                                          | Notes                                                                          |
|------------------------------|--------------------|--------------------------------------------------------------------------------------|--------------------------------------------------------------------------------|
| filters.services.cod.active  |                    | Set to `true` to filter only PPs explicitly marked as supporting CoD.                | Set to `true` to make CoD filter switch selected by default.                   | 
| filters.payments.cash.active |                    | Set to `true` to filter PPs supporting explicitly marked as supporting cash payments | Set to `true` to make Cash Payment filter switch selected by default.          | 
| filters.payments.card.active |                    | Set to `true` to filter PPs supporting explicitly marked as supporting card payments | Set to `true` to make Card Payment filter switch selected by default.          | 
| filters.types.locker.active  |                    | Set to `false` to exclude PPs of this type. Default to `true`.                       | Set to `false` to exclude Lockers and set corresponding checkbox to off.       |
| filters.types.post.active    |                    | Set to `false` to exclude PPs of this type. Default to `true`.                       | Set to `false` to exclude Post Offices and set corresponding checkbox to off.  |
| filters.types.point.active   |                    | Set to `false` to exclude PPs of this type. Default to `true`.                       | Set to `false` to exclude Pickup Points and set corresponding checkbox to off. |

Note, contrary to service and payment type filters, pickup point type filters work opposite and
actually include only specified type of points. To see all available points, all type filters
must be enabled. To see only specific type, set all other type filters to `false` and desired one
to `true`.

#### Callbacks

Widget will call your code back on certain events. In the `callbacks` config branch you set all the
callbacks you want widget to use.

| Parameter          | Description                                                 | Notes |
|--------------------|-------------------------------------------------------------|-------|
| callbacks.onSelect | Called to pass user selected Pickup Point (as JSON object). |       |

#### Other options

| Parameter | Description                                                                          | Notes |
|-----------|--------------------------------------------------------------------------------------|-------|
| codeFirst | Set to `true` to have code provided configuration override any `data-*` attributes.  |       |

NOTE: For filter values, setting initial state to `true` means given feature must be present on PP
to be included in search results, while `false` means that feature is optional. Same for payment
type filters, however in this case it is sufficient for Pickup Point to feature only one of the
requested payment methods (so the logical operation is `OR`).

### Examples

```javascript
const options = {
    api: {
        url: '<URL>',
        accessToken: '<ACCESS-TOKEN>',
        country: 'cz',
        speditions: ["zas","zas-econt-pp"],
    },
    ui: {
        serviceTypeFilters: {
            visible: false,
        }
    },
    callbacks: {
        onSelect: function(item) {
            console.log(`Picked PP: ${JSON.stringify(item)}`);
        },
    }
};
Olza.Widget.embed(options);
```

The same configuration using `data-*` attributes:

```html

<div id="olza-widget"
	 data-olza-api-url="<URL>"
	 data-olza-api-access-token="<ACCESS-TOKEN>"
	 data-olza-api-country="cz"
	 data-olza-api-speditions="zas,zas-econt-pp"
	 data-olza-ui-service-type-filters-visible="false">
</div>
```

```javascript
const options = {
    callbacks: {
        onSelect: function(item) {
            console.log(`Picked PP: ${JSON.stringify(item)}`);
        },
    }
};
Olza.Widget.embed(options);
```

## Notes

* Widget code is NOT reentrant, which means only one instance can run **at the same time**,
  therefore you cannot have both embedded widget and modal on the same page.

* `speditions` is a list of spedition codes requested to be used. Final list may differ as not all
  requested speditions might be available at runtime.

