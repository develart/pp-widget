![Olza Logistic Logo](../src/img/olza-logo.png)

# pp-api-widget #

Olza Logistic's Pickup Point selection widget.

---

## Developer notes

Various notes that can be useful for any devs willing to play with the project on code level.

---

## Debug config switches

The following options are supported when placed in `debug` config branch

| Parameter    | Description                                              | Default       | Notes                                                               |
|--------------|----------------------------------------------------------|---------------|---------------------------------------------------------------------|
| debug.enable | When enabled, the widget might log much more information | `false`       | Do not enable unless needed. Do not enable for production releases. |

## Flags

Country flags are now local SVG images, sourced from [flags-icons](https://github.com/lipis/flag-icons)
package. Icons are cherry picked, yet at some point it might be worth to move this one to regular
dependency once we get more languages supported.

## Spedition imagery

Widget expects both map pin and spedition logos to be located in relative `img/` sub folder and
saved as **SVG** files. The file names must follow the convention `<SPEDITION-CODE>.svg` for pin and
`<SPEDITION-CODE>-logo.svg` for spedition logo.

---

## How to start development

- Copy `webpack.config.js.dist` to `webpack.config.js`. You may want to change `minimize` 
  optimization option to `false` for faster builds while you fiddle.
- Install development dependencies `yarn install --production=false`
  - Turn on files watching `yarn watch`
  - Make changes in any file you need located in `src` directory
  - Reload your page, test your changes and repeat until you are done.
- To build new release version use `yarn build` (remember to stop `yarn watch` first). You
  may want to reenable `minimize` feature for final build for smaller final file.

If you don't want to install node and yarn on your machine you can use `docker-compose.yml` from
repository:

- `docker compose up -d`
- `docker compose exec node yarn watch`
- `docker compose exec node yarn build`

or, if using `docker-compose` binary:

- `docker-compose up -d`
- `docker-compose exec node yarn watch`
- `docker-compose exec node yarn build`

## Auto-checking engine requirements ##
The engine requirements (version of Node.js and yarn) are automatically checked on every run build and watch script. This is handled by the custom script `check-dev-engines.js` which checks the requirements defined in `devEngines` of `package.json` file. The versions can be also defined by ranges by the official npm [version range syntax](https://docs.npmjs.com/about-semantic-versioning#using-semantic-versioning-to-specify-update-types-your-package-can-accept) (for example: `14`, `14.21`, `14.21.3`, `=14.21.3`, `14.x`, `14.21.x`, `~14.21.3`, `^14.21.3`, `14.21.*`, `>14.21.3`, `>= 14.21.3 <15`, etc.).
You can also manually run the command `yarn check-dev-engines` to check the requirements. 

## Adding a prefix to Boostrap 5 CSS classes

To avoid problems using Bootstrap alongside other CSS **we need to add custom namespace and build
our own version**.

- download Bootstrap [source files] (https://getbootstrap.com/docs/5.0/getting-started/download/)
- unzip, open file `../scss/bootstrap.scss`
- wrap all content to class called `widgetbs`:

  .widgetbs {
  @import "functions";
  @import "variables";
  @import "mixins";
  @import "utilities";
  ...
  ..
  .
  }

- to build our customized version use command `npm run dist`
- copy the new file from `../dist/css/bootstrap.min.css`.
