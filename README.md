![Olza Logistic Logo](src/img/olza-logo.png)

# pp-api-widget #

Olza Logistic's Pickup Point selection widget.

---

Table of contents

1. [Usage](#usage)
  1. [Modal picker](#modal-picker)
  1. [Embedded widget](#embedded-widget)
  1. [Important notes](#notes)
1. [Widget configuration](docs/config.md)
1. [Usage examples](#examples)
1. [External links](#links)
1. [Change log](CHANGES.md)
1. [License and credits](#credits)

---

## Usage

Olza Widget can operate in two modes - as modal picker (recommended), that shows up overlaying
current page content and blocks underlying UI unless dismissed or as embedded component, sitting
in your page layout.

### Modal picker

To open modal picker simply call `Olza.Widget.pick(config);` whenever needed. The `config` argument
is widget configuration object. Once user accepts his selection, your `onSelect` callback will be
invoked with detils of selected location. For example:

```javascript
function pick() {
    const options = {
        api: {
            url: '<URL>',
            accessToken: '<ACCESS-TOKEN>',
            country: 'cz',
            speditions: ['zas', 'zas-econt-pp'],
        },
        callbacks: {
            onSelect: function(item) {
                console.log(`Picked PP: ${JSON.stringify(item)}`);
            },
        }
    };
    Olza.Widget.pick(options);
}
```

### Embedded widget

To embed Widget in your layout, put **empty** DIV element with `olza-widget` ID and call
`Olza.Widget.embed(config);` on page load. For example:

```javascript
window.onload = function() {
    const options = {
        api: {
            url: '<URL>',
            accessToken: '<ACCESS-TOKEN>',
            country: 'cz',
            speditions: ['zas', 'zas-econt-pp'],
        },
        callbacks: {
            onSelect: function(item) {
                console.log(`Picked PP: ${JSON.stringify(item)}`);
            },
        },
    };
    Olza.Widget.embed(options);
}
```

---

## Notes

* Widget code is NOT reentrant, which means only one instance can run **at the same time**,
  therefore you cannot have both embedded widget and modal on the same page.

* `speditions` is a list of spedition codes requested to be used. Final list may differ as not all
  requested speditions might be available at runtime.

---

## Examples

See [example/](example/) folder for Widget usage examples.

---

### Links

* BitBucket project website: https://bitbucket.org/develart/pp-widget/
* NPM page: https://www.npmjs.com/package/develart-olzalogistic-pickup-points-widget

---

### Credits

* Copyright &copy; 2022-2025 DevelArt
* Olza Logistic Widget software is open source and licensed
  under [MIT](http://www.opensource.org/licenses/mit-license.php) license.
