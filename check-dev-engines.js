const semver = require('semver')
const package = require('./package.json');

/**
 * Retrieves the version number from the user agent string based on the specified engine.
 *
 * @param {string} engine - The engine name to search for in the user agent string.
 * @return {string|null} The version number extracted from the user agent string, or null if not found.
 */
function getVersionFromUserAgent(engine) {
    const regex = new RegExp(engine + '/v?([0-9]+\.[0-9]+\.[0-9]+)');
    const match = process.env.npm_config_user_agent.match(regex);
    return match ? match[1] : null;
}

// check if package.json contains devEngines
if (!package.devEngines) {
    console.error('Missing devEngines in package.json')
    process.exit(1)
}

// check if all required engines are present
const failedRequirements = []
for (const [engine, versionCheck] of Object.entries(package.devEngines)) {
    switch (engine) {
        case 'node':
        case 'npm':
        case 'yarn':
            // get version of engine
            var version = getVersionFromUserAgent(engine)
            
            if (!version) { // check if the engine is present
                failedRequirements.push(`${engine} is required!`)
            } else if (!semver.satisfies(version, versionCheck)) { // check if the version is supported
                failedRequirements.push(`${engine} version ${version} is not supported! Required version: ${versionCheck}`)
            }
            break
            
        default:
            console.warn(`Warning: Unknown devEngine: ${engine}`)
            break
    }
}

if (failedRequirements.length > 0) {
    console.error('Engine check failed! Some requirements are not met:')
    failedRequirements.forEach((failedRequirement) => console.error(` - ${failedRequirement}`))
    process.exit(1)
}
