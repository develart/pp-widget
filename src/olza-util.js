/*
 * Olza Logistic's Pickup Points selector Widget
 *
 * @author    Marcin Orlowski <marcin.orlowski (#) develart (.) cz>
 * @copyright 2022-2025 DevelArt
 * @license   http://www.opensource.org/licenses/mit-license.php MIT
 * @link      https://bitbucket.org/develart/pp-widget/
 */
import $ from "jquery-slim";

const img = require.context('./img/', false)

/** ***************************************************************************************** **/

const OlzaUtil = {
    /**
     * Returns usable URL tp of spedition's remote map pin image.
     *
     * @param {object} widgetConfig Instance of widget configuration object.
     * @param {string} speditionCode Spedition code to get URL to pin file for.
     *
     * @returns {string} URL to remote spedition's map pin image file.
     */
    getPinImgUrl: function (widgetConfig, speditionCode) {
        speditionCode = speditionCode.trim().toLowerCase();
        return OlzaUtil.buildUrl(widgetConfig, `image/${speditionCode}/pin`);
    },

    /**
     * Returns usable URL to spedition's logo image as served by the API.
     *
     * @param {object} widgetConfig Instance of widget configuration object.
     * @param {string} speditionCode Spedition code to get URL to logo file for.
     *
     * @returns {string} URL to remote spedition's logo image file.
     */
    getLogoImgUrl: function(widgetConfig, speditionCode) {
        speditionCode = speditionCode.trim().toLowerCase();
        return OlzaUtil.buildUrl(widgetConfig, `image/${speditionCode}/logo`);
    },

    /**
     * Returns usable name of spedition feature icon image. Falls back to default image if
     * given feature has no associated image for any reason.
     *
     * @param {string} paymentType String representing payment type as provided by API.
     *
     * @returns {string}
     */
    getPaymentTypeIconFileName: (paymentType) => OlzaUtil.getImgName(`./payment-${paymentType}.svg`, './payment-unknown.svg'),

    /**
     * Returns usable name of service/feature (open24/7, COD support etc) icon image.
     * Falls back to default image if given feature has no associated image for any reason.
     *
     * @param {string} serviceName String representing payment type as provided by API.
     *
     * @returns {string}
     */
    getServiceIconFileName: (serviceName) => OlzaUtil.getImgName(`./service-${serviceName}.svg`, './service-unknown.svg'),

    /**
     * Checks if given mainImg image name matches existing images in img context. If not, returns
     * fallbackImg name. Used to support newly added spedditions that may not have their own icons.
     *
     * NOTE: fallback image MUST exist. There's no extra checks at runtime.
     *
     * @param {string} mainImg     image, i.e './spedition.png'
     * @param {string} fallbackImg fallback image, i.e './default.png'
     *
     * @returns {string}
     */
    getImgName: function(mainImg, fallbackImg) {
        const idx = img.keys().indexOf(mainImg);
        return idx >= 0 ? mainImg : fallbackImg;
    },

    /**
     * JS equivalent of sleep(). Usage (in "async" function only!):
     *
     *    await sleep(1000);
     *
     * @param {int} ms Sleep time in milliseconds
     *
     * @return {Promise<unknown>}
     */
    sleep: (ms) => new Promise(resolve => setTimeout(resolve, ms)),

    /**
     * Constructs full URL to given PP-API end point, with optional query string arguments.
     *
     * @param {object} widgetConfig Instance of widget configuration object.
     * @param {string} endpoint Name of PP API end point (i.e. "find")
     * @param {object} params   Parameters (key: val) to be passed to the API, where key is name
     *                          of API argument (as per API docs, i.e. "access_token")
     *                          NOTE: these vars are in snake_case contrary to widget code that
     *                          uses CamelCase, so beware when crafting your own query string.
     * @returns {string}
     */
    buildUrl: function(widgetConfig, endpoint, params = {}) {
        // Make config copy to avoid mutating original object.
        let opts = {...widgetConfig};

        // Remove all the trailing slashes from base URL if there's any.
        let url = OlzaUtil.dictGet(opts, 'api.url', null);
        if (url !== null) {
            while (url.endsWith('/')) {
                url = url.substring(0, url.length - 1);
            }
            OlzaUtil.dictSet(opts, 'api.url', url);
        }

        const accessToken = OlzaUtil.dictGet(opts, 'api.accessToken', null);
        if (accessToken === null) {
            throw 'Missing valid access_token. Check your configuration.';
        }
        params.access_token = accessToken;

        const items = Object.keys(params).map(function(key) {
            return `${key}=${params[key]}`;
        });

        const baseUrl = OlzaUtil.dictGet(opts, 'api.url');
        return `${baseUrl}/v1/pp/${endpoint}?` + encodeURI(items.join('&'));
    },

    /**
     * Rounds number value to specified number of decimal places.
     *
     * @param {float} value Value to round.
     * @param decimalPlaces Number of places to round fractional values to.
     */
    round: function(value, decimalPlaces = 1) {
        const factorOfTen = Math.pow(10, decimalPlaces);
        return Math.round(value * factorOfTen) / factorOfTen;
    },

    /**
     * Returns configured instance of XHR with Asynchronous work mode.
     *
     * @param {object} widgetConfig Instance of widget configuration object.
     * @param {string} endpoint URL endpoint to use.
     * @param {object} params Parameters to send with request.
     *
     * @returns {XMLHttpRequest}
     */
    getXhr: function(widgetConfig, endpoint, params = {}) {
        const url = OlzaUtil.buildUrl(widgetConfig, endpoint, params);
        const method = 'GET';
        // console.log(`${method}: ${url}`);
        const xhr = new XMLHttpRequest();
        xhr.open(method, url);
        xhr.setRequestHeader('Accept', 'application/json');
        return xhr;
    },

    /** ***************************************************************************************** **/

    /**
     * Returns element referenced by "dot.path" path. If path matches no existing element will
     * either return provided defaultValue, of that argument is not provided, will throw an exception.
     *
     * @param {object} dict Dictionary to read data from
     * @param {string} dotPath String with "dot.path" to target dictionary element
     * @param {*|undefined} defaultValue Optional default value to be returned if no real element is found.
     * @returns {*}
     */
    dictGet: function(dict, dotPath, defaultValue = undefined) {
        const segments = dotPath.split('.');
        let srcNode = dict;
        for (let i = 0; i < segments.length - 1; i++) {
            const segment = segments[i];
            if (!(segment in srcNode)) {
                if (typeof defaultValue === 'undefined') {
                    throw `Invalid SRC path: ${dotPath}`;
                }
                return defaultValue;
            }
            srcNode = srcNode[segment];
        }

        // last segment points to the final element to copy
        const lastSegment = segments[segments.length - 1];
        let val = defaultValue;
        if (lastSegment in srcNode) {
            val = srcNode[lastSegment];
        } else {
            if (typeof defaultValue === 'undefined') {
                throw `Invalid SRC path: ${dotPath}`;
            }
        }
        return val;
    },

    /**
     * Writes value to element referenced by "dot.path" path. Will create all the sub-dictionaries.
     * NOTE: it will overwrite existing target value.
     *
     * @param {object} dict Dictionary to write data to
     * @param {string} dotPath String with "dot.path" to target dictionary element
     * @param {*} val Value to write to target element
     */
    dictSet: function(dict, dotPath, val) {
        const segments = dotPath.split('.');
        let srcNode = dict;
        for (let i = 0; i < segments.length - 1; i++) {
            const segment = segments[i];
            if (!(segment in srcNode)) {
                srcNode[segment] = {};
            }

            const val = srcNode[segment];
            if (val === null || typeof val !== 'object') {
                throw `Invalid SRC path: ${dotPath}`;
            }

            srcNode = val;
        }

        // last segment points to the final element to write to
        const segment = segments[segments.length - 1];

        srcNode[segment] = val;
    },

    /** ***************************************************************************************** **/

    /**
     * Copies element referenced by dotPath from source dictionary to destination. Will throw
     * exception path is not valid. If throw is suppreseed, returns TRUE if copy was successful,
     * FALSE otherwise.
     *
     * @param {object} src Source dictionary to copy data from
     * @param {object} dest Target dictionary to put copied data into
     * @param {string} dotPath String with "dot.path" to target dictionary element
     * @param {boolean} throwOnMissing If TRUE, will throw exception if path is not valid, othewise will return with FALSE..
     */
    copyDictElement: function(src, dest, dotPath, throwOnMissing = true) {
        let srcNode = src;
        let destNode = dest;
        const segments = dotPath.split('.');

        if (segments.length === 0) {
            return false;
        }

        // Walk all the sub-segments except the last one.
        for (let i = 0; i < segments.length - 1; i++) {
            const segment = segments[i];

            // These must be a sub-dictionaries or missing.
            const val = srcNode[segment] || null;
            const valType = typeof val;
            if (val === null) {
                if (throwOnMissing) {
                    throw `Source segment '${segment}' in '${dotPath}' does not exist.`;
                }
                return false;
            }

            if (valType !== 'object') {
                if (throwOnMissing) {
                    throw `Source segment '${segment}' in '${dotPath}' must be a dictionary, '${valType}' found.`;
                }
                return false;
            }

            srcNode = val;

            if (!(segment in destNode)) {
                destNode[segment] = {};
            }
            destNode = destNode[segment];
        }

        // last segment points to the final element to copy
        const lastSegment = segments[segments.length - 1];
        const lastSegmentVal = srcNode[lastSegment];
        if (typeof lastSegmentVal !== 'undefined') {
            destNode[lastSegment] = srcNode[lastSegment];
        }

        return true;
    },

    /**
     * @param src
     * @param dest
     * @param dotPaths
     * @param throwOnMissing
     */
    copyDictElements: function(src, dest, dotPaths, throwOnMissing = true) {
        dotPaths.forEach(dotPath => OlzaUtil.copyDictElement(src, dest, dotPath, throwOnMissing));
    },

    /** ***************************************************************************************** **/

    /**
     * Returns copy of provided object, with only elements present in paths copied. Any path that is invalid (like
     * points to non existing element) is being silently ignored.
     *
     * @param {object} src Source dictionary to copy data from
     * @param {string[]} paths List with "dot.path" formatted paths to copy
     *
     * @returns {object}
     */
    sanitizeDict: function(src, paths) {
        const dest = {};
        // Copy only known elements from user config. We ignore any exception thrown as that
        // simply means given element is not overloaded by the user.
        OlzaUtil.copyDictElements(src, dest, paths, false);
        return dest;
    },

    /** ***************************************************************************************** **/

    /**
     * Recursively walks provided dictionary and returns dotPaths for all the keys encountered.
     *
     * @param {object} dict Dictionary to walk.
     * @param {string[]} result List to hold all the encountered paths.
     * @param {string} parentKey dotPath parent segments of the current node.
     * @returns {string[]}
     */
    dictToDotPaths: function(dict, result = [], parentKey = '') {
        for (let key in dict) {
            const val = dict[key];
            const newKey = `${parentKey}${key}`;
            if (val === null || typeof val !== 'object') {
                result.push(newKey);
                continue;
            }

            if (Object.keys(val).length > 0) {
                OlzaUtil.dictToDotPaths(val, result, `${newKey}.`);
            } else {
                result.push(newKey);
            }
        }

        return result;
    },

    /** ***************************************************************************************** **/

    /**
     * Returns value of selected item from SELECT element, or NULL if none is selected.
     *
     * @param {string} selectId DOM ID of `<SELECT>` element to read.
     *
     * @returns {string|null}
     */
    getSelectedElementValue: function(selectId) {
        /** @type {string|null} */
        let value = $(`#${selectId} option:selected`).val();
        if (typeof value === "undefined" || value === '') {
            value = null;
        }
        return value;
    },

}

/** ********************************************************************************************* **/

export default OlzaUtil
