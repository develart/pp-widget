/*
 * Olza Logistic's Pickup Points selector Widget
 *
 * @author    Marcin Orlowski <marcin.orlowski (#) develart (.) cz>
 * @copyright 2022-2025 DevelArt
 * @license   http://www.opensource.org/licenses/mit-license.php MIT
 * @link      https://bitbucket.org/develart/pp-widget/
 */

/** ********************************************************************************************* **/

/**
 * Semaphore handler.
 */
const OlzaSem = {
    /**
     * List of existing "semaphores".
     *
     * @type {string[]}
     */
    _semaphores: [],

    /**
     * Adds lock for the element referred with unique key. Returns TRUE if
     * lock was successfully obtained (caller is granted access), FALSE
     * if there are other locks for that resource already.
     *
     * @param {string} key Unique semaphore key to set.
     *
     * @returns {boolean}
     */
    get: function(key) {
        let canAccess = false;
        if (OlzaSem._semaphores.indexOf(key) === -1) {
            OlzaSem._semaphores.push(key);
            canAccess = true;
        }
        return canAccess;
    },

    /**
     * Removes lock for the element referred with unique key.
     *
     * @param {string} key Semaphore key to release.
     */
    release: function(key) {
        const keyIdx = OlzaSem._semaphores.indexOf(key);
        if (keyIdx !== -1) {
            OlzaSem._semaphores.splice(keyIdx, 1);
        }
    },

    /** ***************************************************************************************** **/

}

/** ********************************************************************************************* **/

export default OlzaSem
