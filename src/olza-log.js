/*
 * Olza Logistic's Pickup Points selector Widget
 *
 * @author    Marcin Orlowski <marcin.orlowski (#) develart (.) cz>
 * @copyright 2022-2025 DevelArt
 * @license   http://www.opensource.org/licenses/mit-license.php MIT
 * @link      https://bitbucket.org/develart/pp-widget/
 */

import OlzaToast from './olza-toast.js'

/** ********************************************************************************************* **/

const OlzaLog = {
    /**
     * Default log entry prefix.
     */
    prefix: 'OLZA',

    /**
     * Dumps config into log (if debug mode is enabled).
     *
     * @param {string} prefix String prefix appended to each logged entry.
     * @param {object} config Valid configuration object.
     */
    logConfig: function(prefix, config) {
        // FIXME: not updated to new config format yet
        // // As we need to make some changes to the config prior printing,
        // // let's clone original one and mess in a copy.
        // let cfg = {...config};
        //
        // // Hide real access token value if present
        // if ('accessToken' in cfg) {
        //     cfg.accessToken = '***';
        // }
        // // iterate over object properties with keys and values
        // for (const key in cfg) {
        //     if (cfg.hasOwnProperty(key)) {
        //         OlzaLog.log(`${prefix}: ${key} = ${cfg[key]}`);
        //     }
        // }
    },

    /**
     * Processes stack trace to get information about logger invoking method.
     *
     * @returns {string}
     */
    getCallerInfo: function() {
        function getErrorObject() {
            try {
                // noinspection ExceptionCaughtLocallyJS
                throw Error('')
            } catch (err) {
                return err;
            }
        }

        const allFrames = getErrorObject().stack.split('\n');
        const caller = allFrames[4];

        const methodName = caller.split('@')[0];
        const lineNumber = caller.split(':')[2];
        return `${methodName}():${lineNumber}`;
    },

    /**
     * Formats log message and augments with additional information (tags, caller info etc).
     *
     * @param {string} msg Optional message to log.
     * @param {string} tag Optional tag to be added to logger message.
     * @returns {string}
     */
    format: function(msg = '', tag = '') {
        const callerInfo = OlzaLog.getCallerInfo();

        let prefix = OlzaLog.prefix;
        if (prefix !== '') {
            // Mind the trailing space!
            prefix = `[${prefix}] `;
        }

        if (tag !== '') {
            tag += ': ';
        }

        return `${prefix}${tag}${callerInfo} ${msg}`;
    },

    /**
     * Logs a message to the console (if widget debug is enabled).
     *
     * @param {string|object} msg Message to log.
     * @param {string} tag Optional log entry prefix (tag). Default 'DEBUG' string..
     */
    debug: function(msg, tag = 'DEBUG') {
        // FIXME
        // if (typeof Olza.Widget.options !== 'undefined') {
        //     if (!Olza.Widget.options.devDebugEnabled) {
        //         return;
        //     }
        // }

        if (typeof msg === 'object') {
            msg = JSON.stringify(msg);
        }

        const entry = OlzaLog.format(msg, tag);
        console.debug(entry);
    },

    log: (msg, tag = '') => console.log(OlzaLog.format(msg, tag)),

    warn: (msg, tag = 'WARN') => console.warn(OlzaLog.format(msg, tag)),

    permanentError: function(msg, tag = 'ERR') {
        OlzaLog.showPermanentToast(msg);

        OlzaLog.logError(msg, tag);
    },

    error: function(msg, tag = 'ERR') {
        OlzaToast.show(msg);

        OlzaLog.logError(msg, tag);
    },

    logError: function(msg, tag = 'ERR') {
        const entry = OlzaLog.format(msg, tag);
        console.error(entry);
    },

    /**
     * List of already shown permanent toasts.
     */
    permanentToastLog: [],

    /**
     * Shows permanent toast that do not fade away.
     *
     * @param {string} msg
     */
    showPermanentToast: function(msg) {
        // As permanent toasts stay forever, one instance of each message is enough.
        if (OlzaLog.permanentToastLog.indexOf(msg) === -1) {
            OlzaLog.permanentToastLog.push(msg);
            OlzaToast.show(msg, 0);
        }
    },

    /** ***************************************************************************************** **/

}

/** ********************************************************************************************* **/

export default OlzaLog
