/*
 * Olza Logistic's Pickup Points selector Widget
 *
 * @author    Marcin Orlowski <marcin.orlowski (#) develart (.) cz>
 * @copyright 2022-2025 DevelArt
 * @license   http://www.opensource.org/licenses/mit-license.php MIT
 * @link      https://bitbucket.org/develart/pp-widget/
 */

import OlzaLog from './olza-log.js'

/** ********************************************************************************************* **/

const OlzaCache = {
    /**
     * Returns copy of cached PP details or NULL if no cache entry found.
     *
     * @param {string} countryCode Country code of the PP.
     * @param {string} speditionCode Code of PP parent spedition.
     * @param {string} pickupPointId ID of the PP to get details of
     *
     * @returns {Object|null}
     */
    get: function(countryCode, speditionCode, pickupPointId) {
        if (typeof countryCode !== "string" && countryCode.length > 1) {
            OlzaLog.debug('No valid country provided.');
            return null;
        }
        if (typeof speditionCode !== "string" && speditionCode.length > 1) {
            OlzaLog.debug('No valid spedition code provided.');
            return null;
        }
        if (typeof pickupPointId !== "string" && pickupPointId.length > 1) {
            OlzaLog.debug('No valid Pickup Point ID provided.');
            return null;
        }

        const storageKey = `${countryCode}-${speditionCode}-${pickupPointId}`;

        /** @type {string|null} */
        const pp = localStorage.getItem(storageKey);
        return (pp !== null) ? JSON.parse(pp) : null;
    },

    /**
     * TODO: DOCUMENT ME!
     *
     * @param {object} pickupPoint
     */
    set: function(pickupPoint) {
        // const storageKey = `${pp.address.country}-${pp.spedition}-${pp.id}`;
        // localStorage.setItem(storageKey, JSON.stringify(pp));
    },

    /** ***************************************************************************************** **/

}

/** ********************************************************************************************* **/

export default OlzaCache
