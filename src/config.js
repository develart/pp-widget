/*
 * Olza Logistic's Pickup Points selector Widget
 *
 * @author    Marcin Orlowski <marcin.orlowski (#) develart (.) cz>
 * @copyright 2022-2025 DevelArt
 * @license   http://www.opensource.org/licenses/mit-license.php MIT
 * @link      https://bitbucket.org/develart/pp-widget/
 */

import OlzaLog from "./olza-log";

/** ********************************************************************************************* **/

/**
 * This is default widget configuration object.
 *
 * Upon initialization, user provided configuration is merged into copy of these settings
 * and then used at runtime.
 *
 * Some of the configuration elements can also be set via data-* attributes on the root container.
 * See attribute description for details. Fields covered by data-* attributes are marked as such.
 */
const OlzaWidgetDefaultConfig = {

    api: {
        /**
         * OlzaLogistic's API base URL.
         *
         * data-* attribute: data-olza-api-url
         *
         * @type {?string}
         * */
        url: null,

        /**
         * OlzaLogistic's API access token.
         *
         * data-* attribute: data-olza-api-token
         *
         * @type {?string}
         * */
        accessToken: null,

        /**
         * Target country to use library for.
         *
         * data-* attribute: data-olza-api-country
         *
         * @type {string}
         * */
        country: 'cz',

        /**
         * Latitude of location to be shown at start (unless user allows location to be obtained from browser).
         * If NULL (default), fallbacks to location of country's capital city.
         *
         * @type {number|null}
         */
        latitude: null,
        /**
         * Longitude of location to be shown at start (unless user allows location to be obtained from browser).
         * If NULL (default), fallbacks to location of country's capital city.
         *
         * @type {number|null}
         */
        longitude: null,

        /**
         * If TRUE, will try to use browser's location as user starting point on the map.
         *
         * data-* attribute: data-olza-api-use-browser-location
         *
         * @type {boolean}
         */
        useBrowserLocation: true,

        /**
         * This option should probably be named "speditionFilter" as in fact values provided here are used
         * to filter speditions reported by API as available and include only these API knows, that matches
         * values specified here.
         *
         * If you use default value (or set to `[]`) then you will get all speditions that API supports,
         * Because there's nothing explicitely requested.
         *
         * Final list of speditions available at runtime may differ as not all from requested speditions
         * might be available at the time. If empty list is provided,
         * all API available speditions will be used.
         *
         * data-* attribute: data-olza-api-speditions
         *
         * NOTE: When setting this option via data-* attribute, use comma separated **string** to pass
         *       multiple spedition codes.
         *
         * @type {string[]|string}
         */
        speditions: [],

    },

    /** ***************************************************************************************** **/

    /**
     * Controls default state of widget filter UI elements.
     */
    filters: {
        /**
         * Offered services.
         */
        services: {
            cod: {
                /**
                 * Set to TRUE to make CoD filter switch selected by default (meaning, you want
                 * to see ONLY CoD supporting PPs in the results). Default is FALSE (you will
                 * see all).
                 *
                 * @type {boolean}
                 */
                active: false,
            },
        },

        /**
         * Payment type filters.
         */
        payments: {
            cash: {
                /**
                 * Set to TRUE to make CoD payment type filter for CARD selected by default (meaning, you want
                 * to see ONLY PPs that do implicitly announce to support CARD payments for CoD service).
                 * Default is FALSE meaning CARD payment type filter is not taken into consideration.
                 *
                 * @type {boolean}
                 */
                active: false,
            },
            card: {
                /**
                 * Set to TRUE to make CoD payment type filter for CARD selected by default (meaning, you want
                 * to see ONLY PPs that do implicitly announce to support CARD payments for CoD service).
                 * Default is FALSE meaning CARD payment type filter is not taken into consideration.
                 *
                 * @type {boolean}
                 */
                active: false,
            },
        },

        /**
         * Point type filters.
         */
        types: {
            locker: {
                /**
                 * Set to TRUE to make pickup point type filter for LOCKER (BOX) types selected by default
                 * (meaning, you want to see PPs that do implicitly announce to be of this type).
                 * Default is TRUE meaning items of this type are included in search/filters/etc.
                 *
                 * @type {boolean}
                 */
                active: true,
            },
            point: {
                /**
                 * Set to TRUE to make pickup point type filter for POINT (PICKUP POINT) types selected by default
                 * (meaning, you want to see PPs that do implicitly announce to be of this type).
                 * Default is TRUE meaning items of this type are included in search/filters/etc.
                 *
                 * @type {boolean}
                 */
                active: true,
            },
            post: {
                /**
                 * Set to TRUE to make pickup point type filter for POST (POST OFFICE) types selected by default
                 * (meaning, you want to see PPs that do implicitly announce to be of this type).
                 * Default is TRUE meaning items of this type are included in search/filters/etc.
                 *
                 * @type {boolean}
                 */
                active: true,
            },
        },
    },

    /** ***************************************************************************************** **/

    /**
     * Controls aspects of various UI elements.
     */
    ui: {
        /**
         * Id of target DIV to embed widget into (or to create, when running in modal mode).
         */
        divId: 'olza-widget',

        /**
         * Requested UI language (substituted in mergeWithDefaults()).
         * Widget will fall back to browser defined language if this parameter is not not explicitly specified.
         *
         * data-* attribute: data-olza-ui-language
         *
         * @type {?string}
         */
        language: null,

        /**
         * Initial Leaflet map zoom level.
         *
         * data-* attribute: data-olza-ui-initial-zoom
         *
         * @type {number}
         */
        initialZoom: 12,

        /**
         * Zoom level used while "Show on map" button is clicked in PP details pane.
         *
         * data-* attribute: data-olza-ui-details-zoom
         *
         * @type {number}
         */
        detailsZoom: 17,

        /**
         * Default state of service type
         */
        serviceTypeFilters: {
            /**
             * Controls visibility (accessibility) of service type filter UI elements. If set to FALSE,
             * user will not be able to control the state of these filters as UI elements will be hidden
             * and inaccessible. You can still however control the state of these filters via configuration
             * elements in `filters` section.
             *
             * Default is TRUE meaning UI elements are visible.
             *
             * @type {boolean}
             */
            visible: true,
        },
        paymentTypeFilters: {
            /**
             * Controls visibility (accessibility) of payment type (i.e. Card, cash etc) filter UI elements.
             * If set to FALSE, user will not be able to control the state of these filters as UI elements will
             * be hidden and inaccessible. You can still however control the state of these filters via configuration
             * elements in `filters` section.
             *
             * Default is TRUE meaning UI elements are visible.
             *
             * @type {boolean}
             */
            visible: true,
        },

        // speditionTypeFilters: {
        //     /** @type {boolean} */
        //     visible: true,
        // },
    },

    /** ***************************************************************************************** **/

    callbacks: {
        /**
         * Callback called when user picks a pickup point.
         *
         * @type {function}
         */
        onSelect: function() {
            OlzaLog.error('No custom onSelect() callback provided.');
        },
    },

    /** ***************************************************************************************** **/

    /**
     * Developer's corner. Not for the faint of heart.
     */
    debug: {
        /**
         * DO NOT ENABLE FOR PRODUCTION RELEASE!
         *
         * Master switch for all the debug features:
         * * Enables/disables all debug log messages and features.
         * * Must explicitly be enabled for other debug switches to work.
         *
         * @type {boolean}
         */
        enabled: false,
    },

    /** ***************************************************************************************** **/

    /**
     * Determines configuration merge order. If set to TRUE, code configuration object's value
     * override any possible values that come from data-* attributes. If FALSE, data-* attributes
     * got precedence over code configuration object elements.
     *
     * @type {boolean}
     */
    codeFirst: false,

    /** ***************************************************************************************** **/

    /**
     * NOTE: dynamic properties added at runtime:
     *
     * {object} availableSpeditions: {}
     *   Dictionary of available speditions that are going to be used (passed all the filtering etc),
     *   provided as dictionary elements, where the key is spedCode and the value is spedition detailed
     *   data. Content of this dictionary is the result of combining API provided list of available
     *   speditions with user requested ones.
     */

}

/** ********************************************************************************************* **/

export default OlzaWidgetDefaultConfig
