/*
 * Olza Logistic's Pickup Points selector Widget
 *
 * @author    Marcin Orlowski <marcin.orlowski (#) develart (.) cz>
 * @copyright 2022-2025 DevelArt
 * @license   http://www.opensource.org/licenses/mit-license.php MIT
 * @link      https://bitbucket.org/develart/pp-widget/
 */

import $ from "jquery-slim";
import packageJson from '../package.json';

import L from "leaflet";
import 'leaflet/dist/leaflet.css';
import 'leaflet.markercluster';
import 'leaflet.markercluster/dist/MarkerCluster.css'
import 'leaflet.markercluster/dist/MarkerCluster.Default.css'

import '@popperjs/core';
import './plugins/bootstrap/bootstrap.min.css'
import * as bootstrap from 'bootstrap/dist/js/bootstrap.bundle.min.js'

import './plugins/autocomplete/autocomplete.min.css'
import Autocomplete from './plugins/autocomplete/autocomplete.js'

import './css/olza-widget.css'
import OlzaLog from './olza-log.js'
import OlzaSem from './olza-sem.js'
import OlzaUtil from './olza-util.js'
import OlzaCache from './olza-cache.js'
import OlzaConfig from './olza-config.js'
import OlzaLang from './olza-lang.js'

const img = require.context('./img/', false)

const Olza = window.Olza || {};

/** ********************************************************************************************* **/

Olza.Widget = {

    /** ***************************************************************************************** **/

    /**
     * Widget main entry point for modal mode.
     *
     * @param {object} userConfig Initial widget configuration.
     * @param {object|null} clickedElement Element that was clicked on (used to read data-* attributes from)
     */
    pick: (userConfig, clickedElement = null) => Olza.Widget.runWidget(true, userConfig, clickedElement),

    /**
     * Widget main entry point for embedded mode.
     *
     * @param {object} userConfig Initial widget configuration.
     */
    embed: (userConfig) => Olza.Widget.runWidget(false, userConfig),

    /** ***************************************************************************************** **/
    /** ***************************************************************************************** **/
    /** ***                                                                                   *** **/
    /** ***        NO PUBLICLY ACCESSIBLE FUNCTIONS BELOW THIS LINE. PLEASE DISPERSE.         *** **/
    /** ***                                                                                   *** **/
    /** ***************************************************************************************** **/
    /** ***************************************************************************************** **/

    /**
     * Leaflet instance.
     */
    map: null,

    /**
     * Leaflet tiles layer
     *
     * @type {L.LayerGroup}
     */
    mapTiles: null,

    /**
     * Leaflet marker layer
     *
     * @type {L.LayerGroup}
     */
    markers: null,

    /**
     * Debug map layer
     *
     * @type {L.LayerGroup}
     */
    mapDebugLayer: null,

    /**
     * Pin layer
     *
     * @type {L.LayerGroup}
     */
    mapPinLayer: null,

    /**
     * Bootstrap's modal handle
     */
    bootstrapModalHandle: null,

    /**
     * Spedition icon object cache for each speditions.
     *
     * @type {object}
     */
    iconsCache: {},

    /** ***************************************************************************************** **/

    /**
     * Returns configured instance of XHR with Asynchronous work mode using current config.
     *
     * @param {string} endpoint URL endpoint to use.
     * @param {object} params Parameters to send with request.
     *
     * @returns {XMLHttpRequest}
     */
    getXhr: function(endpoint, params = {}) {
        return OlzaUtil.getXhr(Olza.Widget.getOptions(), endpoint, params)
    },

    /**
     * Lang wrapper that honours widget's configuration and returns localized version of string
     * reference by provided key.
     *
     * @param {string} key String key to get localized version of.
     * @returns {string}
     */
    translate: (key) => OlzaLang.get(Olza.Widget.getOptions(), key),

    /**
     * Returns translated spedition name string. Falls back to spedition's label
     * if no translation for specific language is available.
     *
     * @param {object} widgetConfig Instance of widget configuration object.
     * @param {string} speditionCode
     */
    translateSpeditionName: function(speditionCode) {
        let speditionName = null;
        const availableSpeditions = Olza.Widget.getAvailableSpeditions();
        if (speditionCode in availableSpeditions) {
            let sped = availableSpeditions[speditionCode];
            const lang = Olza.Widget.getOption('ui.language');
            if ('names' in sped && lang in sped['names']) {
                speditionName = sped['names'][lang];
            }

            // Let's fallback to sped label.
            if (speditionName === null) {
                speditionName = sped.label;
            }
        }

        if (speditionName === null) {
            speditionName = `??? (${speditionCode})`;
        }

        return speditionName;
    },

    /**
     * Returns string (labale) to be used to refer to specified PickupPoint type referenced
     * by provided typeCode. Will always return a string, even for unknown or with no valid
     * translation code.
     *
     * @param {string} ppTypeCode Code to get the translation for (i.e. "locker", "post", "point".
     *
     * @returns {string}
     */
    translatePpTypeName: function(ppTypeCode) {
        let typeName = null;
        const availableTypes = Olza.Widget.getAvailablePickupPointTypes();
        if (availableTypes !== null) {
            if (ppTypeCode in availableTypes) {
                const translationKey = `filter-pp-type-${ppTypeCode}`;
                typeName = OlzaLang.get(Olza.Widget.getOptions(), translationKey);
            }
        }

        if (typeName === null) {
            typeName = `??? (${ppTypeCode})`;
        }

        return typeName;
    },

    /** ***************************************************************************************** **/

    /**
     * @param {object} options
     */
    setOptions: (options) => Olza.Widget._options = options,

    /**
     * @returns {Object}
     */
    getOptions: () => Olza.Widget._options,

    /**
     * Returns config option referenced by provided "dot.path" or default value if not set. If no default
     * value is provided, raises exception.
     *
     * @param {string} dotPath dotPath String with "dot.path" to target dictionary element
     * @param {*} defaultValue
     */
    getOption: (dotPath, defaultValue = undefined) => OlzaUtil.dictGet(Olza.Widget.getOptions(), dotPath, defaultValue),

    /**
     * Helper to deal with debug config options. Returns given value of debug option
     * **IFF** debugging is enabled in the first place. If 'debug.enabled' is FALSE, will
     * always return default value.
     *
     * @param {string} dotPath dotPath String with "dot.path" to target dictionary element
     * @param {*} defaultValue
     */
    getDebugOption: function(dotPath, defaultValue = undefined) {
        const debugEnabled = Olza.Widget.getOption('debug.enabled', false);
        return debugEnabled ? Olza.Widget.getOption(dotPath, defaultValue) : defaultValue;
    },

    /**
     * @param {string} dotPath dotPath String with "dot.path" to target dictionary element
     * @param {*} value
     */
    setOption: (dotPath, value) => OlzaUtil.dictSet(Olza.Widget.getOptions(), dotPath, value),

    /**
     * Final widget configuration object (once the whole initialization is completed)
     *
     * @type {object}
     */
    _options: {},

    /** ***************************************************************************************** **/

    /**
     * Returns TRUE if widget is running in modal mode, FALSE if is embeded in existing markup.
     *
     * @returns {boolean}
     */
    isModal: () => Olza.Widget._isModal,

    /**
     * @param {boolean} useModalMode
     */
    setWorkMode: (useModalMode) => Olza.Widget._isModal = useModalMode,

    /**
     * If TRUE, the widget runs in modal mode once pick() is called. When false, embed() initializes the widget
     * for embedded mode (i.e., user can pick PPs as many times as s/he wants etc. as doing so will not dismiss
     * the widget etc.
     *
     * @type {boolean}
     */
    _isModal: false,

    /** ***************************************************************************************** **/

    /**
     * Returns most recently clicked/selected Pickup Point defaults.
     *
     * @returns {?object}
     */
    getSelectedPickupPoint: () => Olza.Widget._selectedPickupPoint,
    /**
     * Sets most recently clicked/selected Pickup Point defaults.
     *
     * @param {?object} pp
     */
    setSelectedPickupPoint: (pp) => Olza.Widget._selectedPickupPoint = pp,

    /**
     * Most recently clicked/selected Pickup Point defaults.
     *
     * @type {?object}
     */
    _selectedPickupPoint: null,

    /** ***************************************************************************************** **/

    /**
     * @returns {?object}
     */
    getAvailablePickupPointTypes: () => Olza.Widget._availablePpTypes,

    /**
     * @param {?object} types
     */
    setAvailablePickupPointTypes: (types) => Olza.Widget._availablePpTypes = types,

    /**
     * @type {object}
     */
    _availablePpTypes: null,

    /** ***************************************************************************************** **/

    /**
     * Remaps language code to country code (i.e. 'en' -> 'gb'), needed to build proper name
     * of "flag-icon" package's SVG image.
     *
     * Returns original key if there's either no mapping found or needed (this assumes
     * such image is available - there's no additional fallback nor checks against that.
     *
     * @param {string} key Language code to map.
     *s
     * @returns {string}
     */
    mapFlagCode: function(key) {
        const langFlagExceptions = {
            'en': 'gb', // English -> UK
            'cs': 'cz', // Czech -> Czechia
            'el': 'gr', // Greek -> Greece
            'et': 'ee', // Estonian -> Estonia
        };

        return langFlagExceptions[key] || key;
    },

    /** ***************************************************************************************** **/

    /**
     * If the provided argument is TRUE, returns HTML markup to enforce checked state of the HTML checkbox.
     * Returns empty string otherwise.
     *
     * @param {string} configKey Config key to check for the state value.
     * @returns {string}
     */
    boolToCheckedHtml: (configKey, defaultValue = true) => Olza.Widget.getOption(configKey, defaultValue) ? ' checked="checked" ' : '',

    /**
     * Injects whole widget's layout container referenced by divId config item (for embedded mode).
     * When running, If no such element exists, it will be appended to BODY automatically (modal
     * mode only).
     */
    injectHtml: function() {
        const rootDivId = Olza.Widget.getOption('ui.divId');
        let rootDiv = $(`#${rootDivId}`);

        if (Olza.Widget.isModal()) {
            rootDiv.remove();
            $('body').append(`<div id="${rootDivId}" class="widgetbs"><div id="olza-widget-main" class="modal"></div></div>`);
        } else {
            if (rootDiv.length === 1) {
                $(`#${rootDivId}`).append(`<div class="widgetbs"><div id="olza-widget-main"></div></div>`);
            } else {
                OlzaLog.logError(`Needs single "${rootDivId}" DIV for embedding, found ${rootDiv.length}.`);
                return false;
            }
        }

        const nearbyInitialMessage = Olza.Widget.translate('nearby-initial-message');

        const widgetContentHtml = `
            <div class="container-fluid">
                <div class="row align-items-center justify-content-center">
                    <div id="olza-logo" class="col-7 col-sm-7 align-self-start text-start">
                         <img src="${img('./olza-logo.png')}" alt="Olza Logistic" class="pl-2 pl-sm-0" />
                    </div>
                    
                    <!-- language selector dropdown -->
                    <div id="olza-language-selector" class="col-5 col-sm-5 d-flex justify-content-end align-items-center text-end">
                        <div class="btn-group">
                            <button type="button" class="btn dropdown-toggle" data-bs-toggle="dropdown" aria-expanded="false">
                                <span id="olza-language-selector-current"></span>
                            </button>
                            <ul id="olza-language-selector-languages" class="dropdown-menu">
                            </ul>
                        </div>
                    </div>

                <!-- olza-main -->
                <div id="olza-main" class="row pb-3">
                
                    <!-- olza-left -->
                    <div id="olza-left" class="col-md-5 col-lg-4 col-xl-3">
                        <div id="olza-search" class="w-100">
                            <div id="olza-search-busy" class="olza-busy"><img src="${img('./loader.gif')}" alt="Busy state indicator" /></div>
                            <div id="olza-search-form-container">
                                <form id="olza-search-from" method="get" action="#" class="position-relative search">
                                    <select id="olza-search-spedition-select" class="form-select"></select>
                                    
                                    <div class="position-relative">
                                        <div class="auto-search-wrapper max-height">
                                            <input class="form-control w-100 mt-3" type="text" id="olza-map-search-input" autocomplete="off">
                                        </div>
                                    </div>
                                    
                                    <span id="olza-search-toggle-additional-filters-button" class="olza-search-filters-button">???</span>
                                    
                                    <div id="olza-search-filter-container" class="olza-search-filters-container mt-2">
                                        <div id="olza-search-filters" class="olza-search-filters-item p-2">
                                            <div id="olza-search-filter-service-cod" class="olza-service-type-filter-item form-check form-switch form-check-inline mt-1">
                                                <input class="form-check-input" type="checkbox" ${Olza.Widget.boolToCheckedHtml('filters.services.cod.active', false)} role="switch" id="olza-search-filter-service-cod-state" title="${Olza.Widget.translate('cod-available')}">
                                                <label class="form-check-label" for="olza-search-filter-service-cod-state"><img class="icon" src="${img(OlzaUtil.getServiceIconFileName('cod'))}" title="${Olza.Widget.translate('cod-available')}" /></label>
                                            </div>
                                            <div id="olza-search-filter-payment-cash" class="olza-payment-type-filter-item form-check form-switch form-check-inline mt-1">
                                                <input class="form-check-input" type="checkbox" ${Olza.Widget.boolToCheckedHtml('filters.payments.cash.active', false)} role="switch" id="olza-search-filter-payment-cash-state" title="${Olza.Widget.translate('payment-type-cash')}">
                                                <label class="form-check-label" for="olza-search-filter-payment-cash-state"><img class="icon" src="${img(OlzaUtil.getPaymentTypeIconFileName('cash'))}" title="${Olza.Widget.translate('payment-type-cash')}" /></label>
                                            </div>
                                            <div id="olza-search-filter-payment-card" class="olza-payment-type-filter-item form-check form-switch form-check-inline mt-1">
                                                <input class="form-check-input" type="checkbox" ${Olza.Widget.boolToCheckedHtml('filters.payments.card.active', false)} role="switch" id="olza-search-filter-payment-card-state" title="${Olza.Widget.translate('payment-type-card')}">
                                                <label class="form-check-label" for="olza-search-filter-payment-card-state"><img class="icon" src="${img(OlzaUtil.getPaymentTypeIconFileName('card'))}" title="${Olza.Widget.translate('payment-type-card')}" /></label>
                                            </div>
                                            <div id="olza-search-filter-payment-online" class="olza-payment-type-filter-item form-check form-switch form-check-inline mt-1">
                                                <input class="form-check-input" type="checkbox" ${Olza.Widget.boolToCheckedHtml('filters.payments.online.active', false)} role="switch" id="olza-search-filter-payment-online-state" title="${Olza.Widget.translate('payment-type-online')}">
                                                <label class="form-check-label" for="olza-search-filter-payment-online-state"><img class="icon" src="${img(OlzaUtil.getPaymentTypeIconFileName('online'))}" title="${Olza.Widget.translate('payment-type-online')}" /></label>
                                            </div>
                                        </div>
                                        
                                        <div id="olza-search-filters" class="olza-search-filters-item p-2 mt-2">
                                            <div id="olza-search-filter-pp-type-locker" class="olza-search-filter-pp-type-item form-check form-switch form-check-inline mt-1">
                                                <input class="form-check-input" type="checkbox" ${Olza.Widget.boolToCheckedHtml('filters.types.locker.active')} role="switch" id="olza-search-filter-pp-type-locker-state">
                                                <label class="form-check-label" id="olza-search-filter-pp-type-locker-label" for="olza-search-filter-pp-type-locker-state">???</label>
                                            </div>    
                                            
                                            <div id="olza-search-filter-pp-type-point" class="olza-search-filter-pp-type-item form-check form-switch form-check-inline mt-1">
                                                <input class="form-check-input" type="checkbox"  ${Olza.Widget.boolToCheckedHtml('filters.types.point.active')} role="switch" id="olza-search-filter-pp-type-point-state">
                                                <label class="form-check-label" id="olza-search-filter-pp-type-point-label" for="olza-search-filter-pp-type-point-state">???</label>
                                            </div>
                                            
                                            <div id="olza-search-filter-pp-type-post" class="olza-search-filter-pp-type-item form-check form-switch form-check-inline mt-1">
                                                <input class="form-check-input" type="checkbox"  ${Olza.Widget.boolToCheckedHtml('filters.types.post.active')} role="switch" id="olza-search-filter-pp-type-post-state">
                                                <label class="form-check-label" id="olza-search-filter-pp-type-post-label" for="olza-search-filter-pp-type-post-state">???</label>
                                            </div>
                                        </div>
                                    </div>    
        
                                </form>
                            </div>
                        </div>
                        
                        <div class="nearby-wrap">
                            <div id="olza-nearby-busy" class="olza-busy"><img src="${img('./loader.gif')}" alt="Busy state indicator" /></div>
                            <input id="collapsible" class="toggle" type="checkbox"> <label for="collapsible" class="lbl-toggle" id="olza-pp-closest-point-label"></label>
                            <div class="collapsible-content">
                                <div id="olza-nearby">
                                    <div id="olza-closest-points-list-empty-label">${nearbyInitialMessage}</div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- end of olza-left -->
                    
                    <!-- olza-right -->
                    <div id="olza-right" class="col-md-7 col-lg-8 col-xl-9">
                        <div class="d-flex flex-column h-100">
                            <div class="row flex-grow-1">
                                <div id="olza-map" class="w-100"><div id="olza-map-busy" class="olza-busy"><img src="${img('./loader.gif')}" alt="Busy state indicator" /></div></div>
                            </div>
                            <div class="row">
                                <div id="olza-pp-details" class="w-100">
                                    <div id="olza-pp-details-busy" class="olza-busy"><img src="${img('./loader.gif')}" alt="Busy state indicator" /></div>
                                    <div id="olza-pp-details-box">
                                        <div class="col-3 col-sm-2 col-lg-2"><img id="olza-pp-details-logo" alt="Logo"/></div>
                                        <div class="col-9 col-sm-6 col-lg-7 pt-2">
                                            <div id="olza-pp-details-info" />
                                            <div id="olza-pp-details-service-icons" />
                                        </div>
                                        <div class="col-12 col-sm-4 col-lg-3 mt-2 mt-sm-0" id="olza-pp-details-hours" />
                                        <div id="olza-pp-details-close-button" class="olza-pp-details-close-button">
                                            <svg xmlns="http://www.w3.org/2000/svg" width="22" height="22" fill="currentColor" class="bi bi-x-circle" viewBox="0 0 16 16">
                                                <path d="M4.646 4.646a.5.5 0 0 1 .708 0L8 7.293l2.646-2.647a.5.5 0 0 1 .708.708L8.707 8l2.647 2.646a.5.5 0 0 1-.708.708L8 8.707l-2.646 2.647a.5.5 0 0 1-.708-.708L7.293 8 4.646 5.354a.5.5 0 0 1 0-.708z"/>
                                            </svg>
                                        </div>
                                    </div>
                                    <div id="olza-pp-details-buttons" class="text-center">
                                        <button id="olza-pp-details-show-on-map-button" type="button" class="btn btn-info mt-2 mx-1">
                                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-map" viewBox="0 0 16 16">
                                                <path fill-rule="evenodd" d="M15.817.113A.5.5 0 0 1 16 .5v14a.5.5 0 0 1-.402.49l-5 1a.502.502 0 0 1-.196 0L5.5 15.01l-4.902.98A.5.5 0 0 1 0 15.5v-14a.5.5 0 0 1 .402-.49l5-1a.5.5 0 0 1 .196 0L10.5.99l4.902-.98a.5.5 0 0 1 .415.103zM10 1.91l-4-.8v12.98l4 .8V1.91zm1 12.98 4-.8V1.11l-4 .8v12.98zm-6-.8V1.11l-4 .8v12.98l4-.8z"/>
                                            </svg>
                                            <span id="olza-pp-details-show-on-map-button-label"></span>
                                        </button>
                                        <button id="olza-pp-details-pick-button" type="button" class="btn btn-success mt-2 mx-1">
                                            <!-- icon: check-circle -->
                                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-check-circle" viewBox="0 0 16 16">
                                                <path d="M8 15A7 7 0 1 1 8 1a7 7 0 0 1 0 14zm0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16z"/>
                                                <path d="M10.97 4.97a.235.235 0 0 0-.02.022L7.477 9.417 5.384 7.323a.75.75 0 0 0-1.06 1.06L6.97 11.03a.75.75 0 0 0 1.079-.02l3.992-4.99a.75.75 0 0 0-1.071-1.05z"/>
                                            </svg>
                                            <span id="olza-pp-details-pick-button-label"></span>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- end of olza-right -->
                </div>
                <!-- end of olza-main -->
            </div>
        `;

        let htmlToInject = '';
        if (Olza.Widget.isModal()) {
            htmlToInject = `
                <div class="modal-dialog modal-xl">
                    <div class="modal-content">
                        <div class="modal-header">
                            <span id="olza-widget-title" class="modal-title"></span>
                            <button id="olza-modal-header-close-button" type="button" class="btn-close"></button>
                        </div>
                        <!-- content -->
                        ${widgetContentHtml}
                        <!-- end of content -->
                    </div>
                    <!-- end of modal-content -->
                </div>
            `;
        } else {
            htmlToInject = ` 
                <div class="modal-content">
                    <div class="modal-header">
                        <span id="olza-widget-title" class="modal-title"></span>
                    </div>
                    <!-- content -->
                    ${widgetContentHtml}
                    <!-- end of content -->
                </div>
                <!-- end of modal-content -->
            `;
        }

        $('#olza-widget-main').html(htmlToInject);

        if (Olza.Widget.isModal()) {
            Olza.Widget.bootstrapModalHandle = new bootstrap.Modal(document.getElementById('olza-widget-main'), {
                backdrop: 'static',
                keyboard: true,
                focus: true,
                show: true
            });
        }

        // Search autocomplete
        new Autocomplete('olza-map-search-input', {
            insertToInput: true,
            onSearch: ({currentValue}) => Olza.Widget.autocompleteOnSearch({currentValue}),
            onSubmit: ({index, object}) => {
                /** @type {object} */
                const pp = Olza.Widget.getAutocompleteItems()[index];
                Olza.Widget.showDetails(pp.address.country, pp.spedition, pp.id);
            },
            onReset: () => {
                Olza.Widget.setAutocompleteItems([]);
            },
            onResults: ({currentValue, matches, template, classGroup}) =>
                Olza.Widget.autocompleteOnResults({currentValue, matches, template, classGroup}),
            noResults: ({template}) => template('<li>' + Olza.Widget.translate('search-result-not-found') + '</li>'),
        });

        // Filters UI
        const serviceFiltersVisibility = Olza.Widget.getOption('ui.serviceTypeFilters.visible');
        $('.olza-service-type-filter-item').toggle(serviceFiltersVisibility);

        const paymentsTypeFiltersVisibility = Olza.Widget.getOption('ui.paymentTypeFilters.visible');
        $('.olza-payment-type-filter-item').toggle(paymentsTypeFiltersVisibility);

        // modal click handlers
        if (Olza.Widget.isModal()) {
            $('#olza-modal-header-close-button').click(Olza.Widget.close);
        }

        // details pane click handlers
        $('#olza-pp-details-close-button').click(Olza.Widget.closeDetailsPane);
        $('#olza-pp-details-show-on-map-button').click(Olza.Widget.showSelectedPointOnMap);
        $('#olza-pp-details-pick-button').click(Olza.Widget.pickThisPoint);

        const checkboxIds = [
            '#olza-search-filter-service-cod-state',
            '#olza-search-filter-payment-cash-state',
            '#olza-search-filter-payment-card-state',
            '#olza-search-filter-payment-online-state',
            '#olza-search-filter-pp-type-locker-state',
            '#olza-search-filter-pp-type-point-state',
            '#olza-search-filter-pp-type-post-state'
        ];
        checkboxIds.forEach((checkboxId) => $(checkboxId).click(Olza.Widget.refineNearby));

        $('#olza-search-filter-container').hide();
        $('#olza-search-toggle-additional-filters-button').click(() => {
            $('#olza-search-filter-container').toggle();
            // calculate and set the remaining height to fill viewport
            const minPadding = 235;
            const containerHeight = Math.round($('#olza-search-form-container').outerHeight());
            $('#olza-nearby').css('height', `calc(100vh - ${containerHeight + minPadding}px)`);
         });

        return true;
    },

    /** ***************************************************************************************** **/

    /** @returns {string[]} */
    getAutocompleteItems: () => Olza.Widget._autocompleteItems,

    /**
     * @param {string[]} items
     */
    setAutocompleteItems: (items) => Olza.Widget._autocompleteItems = items,

    /** @type {string[]} */
    _autocompleteItems: [],

    /**
     * Process fetched suggestions to be shown to the users. Returns HTML.
     */
    autocompleteOnResults: ({currentValue, matches, template, classGroup}) => {
        return matches === 0
            ? template
            : Olza.Widget.getAutocompleteItems().map((pp) => {
                const regex = new RegExp(currentValue, "gi");
                const logoUrl = OlzaUtil.getLogoImgUrl(Olza.Widget.getOptions(), pp.spedition);
                return `<li>
                            <div style="margin-bottom: 10px;" class="olza-autocomplete-item">
                                ${pp.names[0].replace(regex, (str) => `<span class="highlight">${str}</span>`)}
                            </div>
                            <div style="display: flex;" class="olza-autocomplete-item">
                                <div>
                                    <img class="logo" src="${logoUrl}" alt="${pp.spedition} Logo">
                                </div>
                                <div>
                                    <div class="address">
                                        <small>
                                            ${pp.address.full.replace(regex, (str) => `<span class="highlight">${str}</span>`)}
                                        </small>
                                    </div>
                                </div>
                            </div>
                        </li>`;
            }).join('');
    },

    /**
     * Handles fetching autocomplete suggestions.
     */
    autocompleteOnSearch: function({currentValue}) {
        const findParams = {
            country: Olza.Widget.getOption('api.country'),
            limit: 10,
            spedition: Olza.Widget.getSelectedSpeditionsAsString(),
            services: Olza.Widget.getServiceFiltersAsString(),
            payments: Olza.Widget.getPaymentTypeFiltersAsString(),
            types: Olza.Widget.getSelectedPickupPointTypeFiltersAsString(),
        };

        currentValue = currentValue || '';
        if (currentValue.trim() !== '') {
            findParams.search = currentValue;
        }

        const api = OlzaUtil.buildUrl(Olza.Widget.getOptions(), 'find', findParams);
        return new Promise((resolve) => {
            fetch(api)
                .then((response) => response.json())
                .then((data) => {
                    Olza.Widget.setAutocompleteItems(data.data.items);
                    resolve(data.data.items);
                })
                .catch((error) => {
                    OlzaLog.error(error);
                });
        });
    },

    /** ***************************************************************************************** **/

    /**
     * Initializes and preconfigures widget using defaults. Then fetches widget config params
     * and environment details and calls prepareWidgetWithLoadedConfig() to finish the initialization
     * and run the widget.
     *
     * @param {boolean} useModalMode Set to TRUE to run in modal mode, FALSE to run as embedded widget.
     * @param {object} userConfig Initial widget configuration.
     * @param {object|null} clickedElement Element that was clicked on (used to read data-* attributes from).
     */
    runWidget: function(useModalMode, userConfig, clickedElement = null) {
        Olza.Widget.setWorkMode(useModalMode);

        Olza.Widget.initFindApiQueryCounters();

        // This is to "boot" widget defaults, so all internals will have some initial state regardless
        // of user config. This is temporary config and it will be then replaced by merged configuration.
        Olza.Widget.setOptions(OlzaConfig.getDefaults());

        // Configuration step 1 of 2: Lay config foundation by merging user config with widget's defaults.
        const finalConfig = OlzaConfig.init(useModalMode, userConfig, clickedElement);
        Olza.Widget.setOptions(finalConfig);

        if (Olza.Widget.injectHtml()) {
            $("#olza-search-from").on('submit', (event) => event.preventDefault());
        } else {
            OlzaLog.error('Failed to initialize widget');
            return;
        }
        Olza.Widget.closeDetailsPane();

        // Let user know we're busy.
        Olza.Widget.lockWidget();

        // Now fetch "config-internal/" from API to continue widget configuration

        // Next we need to fetch available speditions from API. This would be config step 2 of 2. Once data is
        // fetched and merged into main config, the UI will be updated to show spedition names. Query API for
        // available speditions and other parameters required by the widget to operate properly. Once config
        // is downloaded and processed the configuration will continue.
        let configXhr = Olza.Widget.getXhr('config-internal', {
            widgetVersion: Olza.Widget.getVersionString(),
            country: finalConfig.api.country,
        });
        configXhr.onload = function() {
            const json = JSON.parse(configXhr.responseText);
            Olza.Widget.prepareWidgetWithLoadedConfig(json);
        }
        configXhr.onerror = function() {
            OlzaLog.debug(JSON.stringify(configXhr.responseText));
            OlzaLog.error(Olza.Widget.translate('config-request-failed'));
            // We keep widget locked at this point to force user
            // to close it and re-open.
        };

        configXhr.send();

        // Configuration continues in prepareWidgetWithLoadedConfig() invoked from XHR callback
    },

    /**
     * Step 2/2 of Widget configuration process. Invoked after API provided "config-internal/" is
     * returned and successfully processed. Once all systems are setup widget will eventually run.
     *
     * @param {object} json PP API response for "config-internal/" request
     */
    prepareWidgetWithLoadedConfig: function(json) {
        if (typeof json === 'undefined' || json.constructor !== Object) {
            const errMsg = Olza.Widget.translate('no-valid-config-received-from-api');
            OlzaLog.permanentError(errMsg);
            throw errMsg;
        }
        if (!('success' in json && json.success)) {
            OlzaLog.permanentError(Olza.Widget.translate('no-valid-config-received-from-api'));
            throw errMsg;
        }

        // Available PP types to be used for filtering.
        const availablePpTypes = OlzaUtil.dictGet(json, 'data.points.types', {});
        Olza.Widget.setAvailablePickupPointTypes(availablePpTypes);

        // Intersect requested and available speditions to compute final set.
        // Check user requested speditions first
        let userRequestedSpeds = Olza.Widget.getOption('api.speditions');
        if (userRequestedSpeds.length === 0) {
            // If none is explicitly requested, serve all we have available.
            userRequestedSpeds = Object.keys(OlzaUtil.dictGet(json, 'data.speditions', {}));
        }

        // Number of speditions requested (either by the user or by API fallback)
        if (userRequestedSpeds.length === 0) {
            const errMsg = Olza.Widget.translate('no-valid-speditions-found');
            OlzaLog.permanentError(errMsg);
            throw errMsg;
        }

        // Collect spedition data for all the speditions that are qualified as supported by API
        let availableSpeditionsFiltered = {};
        const configResponseJson = json.data;
        const configSpeditions = OlzaUtil.dictGet(configResponseJson, 'speditions', null);
        if (configSpeditions === null) {
            const errMsg = Olza.Widget.translate('no-valid-speditions-found');
            OlzaLog.permanentError(errMsg);
            throw errMsg;
        }
        userRequestedSpeds.forEach(function(spedCode) {
            spedCode = spedCode.trim().toUpperCase();
            const spedDetails = configSpeditions[spedCode] || null;
            if (spedDetails !== null && !(spedCode in availableSpeditionsFiltered)) {
                availableSpeditionsFiltered[spedCode] = configResponseJson.speditions[spedCode];
            }
        });

        // Abort if we are ended up with no spedition codes to use.
        if (Object.keys(availableSpeditionsFiltered).length === 0) {
            const errMsg = Olza.Widget.translate('no-valid-speditions-found');
            OlzaLog.permanentError(errMsg);
            throw errMsg;
        }

        Olza.Widget.setAvailableSpeditions(availableSpeditionsFiltered);

        /// ----

        let currentLang = Olza.Widget.getOption('ui.language');
        if (!OlzaLang.isLanguageSupported(currentLang)) {
            currentLang = OlzaLang.getBrowserLanguage();
            if (!OlzaLang.isLanguageSupported(currentLang)) {
                currentLang = OlzaLang.getDefaultLanguage();
            }
            Olza.Widget.setOption('ui.language', currentLang);
        }

        Olza.Widget.setLanguage(currentLang);
        Olza.Widget.initLanguageSelector(currentLang);

        Olza.Widget.populateSpeditionSelect();

        // We do not need to manually update search results here as populating
        // spedition will trigger onChange event which will do that for us anyway.

        let initLatitude = Olza.Widget.getOption('api.latitude')
        let initLongitude = Olza.Widget.getOption('api.longitude')

        // if no initial geo-location is given, let's use capital city's:
        if (initLatitude === null || initLongitude === null) {
            const capitalCities = {
                'bg': [42.683333, 23.316667],   // Bulgaria: Sofia
                'cz': [50.08804, 14.42076],     // Czechia: Praha
                'ee': [59.416667, 24.75],       // Estonia: Tallin
                'gr': [37.966667, 23.716667],   // Grecce: Athens
                'hr': [45.8, 16],               // Croatia: Zagreb
                'hu': [47.433333, 19.25],       // Hungary: Budapest
                'lt': [54.683333, 25.316667],   // Lithuania: Vilnus
                'lv': [56.95, 24.1],            // Latvia: Riga
                'pl': [52.216667, 21.033333],   // Poland: Warsaw
                'ro': [44.416667, 26.1],        // Romania: Bucharest
                'si': [46.05, 14.5],            // Slovenia: Ljubliana
                'sk': [48.15, 17.116667],       // Slovakia: Bratislava
                'ua': [50.433333, 30.516667],   // Ukraine: Kiev
            };

            const countryCode = Olza.Widget.getOption('api.country')
            // TODO: fallback should probably use country from default, built-in config instead.
            const country = countryCode in capitalCities ? countryCode : 'cz';
            initLatitude = capitalCities[country][0];
            initLongitude = capitalCities[country][1];
        }
        const initLocation = L.latLng(initLatitude, initLongitude);

        // Try to get location from the browser.
        if (Olza.Widget.useBrowserLocation) {
            Olza.Widget.showCurentBrowserLocation();
        }

        let leafletOpts = {
            // center: initLocation,
            // errorTileUrl: ''
            zoom: Olza.Widget.getOption('ui.initialZoom', 12),
            center: initLocation,
        };

        // Map container MUST be visible prior adding map instance, otherwise map viewport will be incorrectly measured.
        Olza.Widget.open();

        Olza.Widget.map = L.map('olza-map', leafletOpts);
        Olza.Widget.map._layersMaxZoom = 19;
        Olza.Widget.map.on('click', (event) => Olza.Widget.updateNearby(event.latlng));

        // Can't listen to 'moveend' as the map can emit that event just after we increment the counter, causing instant load.
        // Olza.Widget.map.on('movestart', (event) => Olza.Widget.reloadPickupPointsOnFirstMapEvent());
        // Olza.Widget.map.on('resize', (event) => Olza.Widget.reloadPickupPointsOnFirstMapEvent());
        // Olza.Widget.map.on('zoomend', (event) => Olza.Widget.reloadPickupPointsOnFirstMapEvent());

        Olza.Widget.map.setView(initLocation, Olza.Widget.getOption('ui.initialZoom', 12));
        Olza.Widget.markers = L.markerClusterGroup({
            chunkedLoading: true
        });
        Olza.Widget.map.addLayer(Olza.Widget.markers);

        // Click debug layer
        Olza.Widget.mapDebugLayer = L.layerGroup();
        Olza.Widget.map.addLayer(Olza.Widget.mapDebugLayer);

        Olza.Widget.mapPinLayer = L.layerGroup();
        Olza.Widget.map.addLayer(Olza.Widget.mapPinLayer);

        $("#olza-search-spedition-select").on('change', function(_event, _ui) {
            if (Olza.Widget.isOnSpeditionChangeListenerEnabled()) {
                // Spedition selector change must clear otherwise we'd end up risking having empty search result set.
                $('#olza-map-search-input').val('');

                // Run API search and then triggers nearby update
                Olza.Widget.doApiSearchQueryFromUi(true, false);

                // Stop propagating.
                return false;
            }
        });

        Olza.Widget.mapTiles = L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
            attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
        });
        Olza.Widget.map.addLayer(Olza.Widget.mapTiles)

        // Run quick initial search with results clipped to visible map bounds and trigger
        // PP prefetch once initial loading is completed.
        Olza.Widget.doApiSearchQueryFromUi(true, true, Olza.Widget.prefetchAllCountryPPs);

        Olza.Widget.unlockWidget();
    },

    prefetchAllowed: true,

    prefetchAllCountryPPs: function() {
        Olza.Widget.prefetchAllowed = false;

        const search = $("#olza-map-search-input").val();
        const speditions = Olza.Widget.getSelectedSpeditions();
        const services = Olza.Widget.getServiceFilters();
        const payments = Olza.Widget.getPaymentTypeFilters();
        const mapBounds = null;
        const ppTypeFilter = Olza.Widget.getSelectedPickupPointTypeFilters();

        const findXhr = Olza.Widget.getApiFindXhr(speditions, search, services, payments, mapBounds, ppTypeFilter);
        findXhr.onload = function() {
            Olza.Widget.processFindApiResponse(this, false, false);
        }
        findXhr.onerror = function() {
            OlzaLog.error(Olza.Widget.translate('find-request-failed'));
        }
        findXhr.send();
    },

    /** ***************************************************************************************** **/

    /** @returns {boolean} */
    isOnPickupPointTypeFilterChangeListenerEnabled: () => Olza.Widget._onPickupPointTypeFilterChangeListener,

    /**
     * Enables spedition selector change listener.
     */
    enableOnPickupPointTypeFilterChangeListener: () => Olza.Widget._onPickupPointTypeFilterChangeListener = true,

    /**
     * Disables spedition selector change listener.
     */
    disableOnPickupPointTypeFilterChangeListener: () => Olza.Widget._onPickupPointTypeFilterChangeListener = false,

    /**
     * Controls event lister reacting on spedition selector changes. Usually used to numb it for
     * the language change, otherwise it would trigger "change" event causing search and nearby
     * calls to be made, which is not what we want in that particular case.
     *
     * @type {boolean}
     */
    _onPickupPointTypeFilterChangeListener: true,

    /** ***************************************************************************************** **/

    /** @returns {boolean} */
    isOnSpeditionChangeListenerEnabled: () => Olza.Widget._onSpeditionChangeListenerEnabled,

    /**
     * Enables spedition selector change listener.
     */
    enableOnSpeditionChangeListener: () => Olza.Widget._onSpeditionChangeListenerEnabled = true,

    /**
     * Disables spedition selector change listener.
     */
    disableOnSpeditionChangeListener: () => Olza.Widget._onSpeditionChangeListenerEnabled = false,

    /**
     * Controls event lister reacting on spedition selector changes. Usually used to numb it for
     * the language change, otherwise it would trigger "change" event causing search and nearby
     * calls to be made which is not what we want in that particular case.
     *
     * @type {boolean}
     */
    _onSpeditionChangeListenerEnabled: true,

    /** ***************************************************************************************** **/

    /**
     * Fetches nearby PPs relative to provided location.
     *
     * @param {LatLng|null} latLng Reference location to look for nearbys around it.
     * @param {object|null} excludePP Pickup point to be excluded from shown nearby pickup points list.
     * @param {function|null} callback Callback to be called after nearby pickup points are fetched.
     */
    updateNearby: function(latLng, excludePP = null, callback = null) {
        if (latLng === null) {
            return;
        }

        if (Olza.Widget.getDebugOption('debug.showMapClicks', false)) {
            Olza.Widget.mapDebugLayer.clearLayers();
            L.circle(latLng, {color: 'red', radius: 3000, weight: 1}).addTo(Olza.Widget.mapDebugLayer);
        }

        // Draw pin at the nearby center point.
        const pin = L.icon({
            iconUrl: img('./pin.svg'),
            iconSize: [80, 80], // size of the pin image
            iconAnchor: [40, 80], // point of the icon which will correspond to marker's location
            popupAnchor: [0, -80] // point from which the popup should open relative to the iconAnchor
        });
        const marker = L.marker(latLng, {
            title: latLng,
            icon: pin
        });
        // Handle direct click on the marker to show details of clicked PP.
        Olza.Widget.mapPinLayer.clearLayers();
        Olza.Widget.mapPinLayer.addLayer(marker);

        // Refresh PPs
        const lockKey = 'olza-nearby-update';
        if (!OlzaSem.get(lockKey)) {
            return;
        }

        const fields = ['name', 'address', 'location', 'services'].join(',');
        const nearbyXhr = Olza.Widget.getXhr('nearby', {
            widgetVersion: Olza.Widget.getVersionString(),
            country: Olza.Widget.getOption('api.country'),
            spedition: Olza.Widget.getSelectedSpeditionsAsString(),
            location: `${latLng.lat},${latLng.lng}`,
            fields: fields,
            services: Olza.Widget.getServiceFiltersAsString(),
            payments: Olza.Widget.getPaymentTypeFiltersAsString(),
            types: Olza.Widget.getSelectedPickupPointTypeFiltersAsString(),
            limit: 15
        });

        nearbyXhr.callback = callback;

        nearbyXhr.onload = function() {
            const json = JSON.parse(this.responseText);
            excludePP = excludePP || null;
            Olza.Widget.processNearbyResponse(json, excludePP)
            Olza.Widget.unlockNearbyContainer();
            OlzaSem.release(lockKey);
            if (this.callback !== null) {
                this.callback();
            }
        };
        nearbyXhr.onerror = function() {
            OlzaLog.error(Olza.Widget.translate('nearby-request-failed'));
            Olza.Widget.unlockNearbyContainer();
            OlzaSem.release(lockKey);
        };

        Olza.Widget.lockNearbyContainer();
        // If no ppType filter checkbox is selected, then we do not want to do any API searches, because
        // optional filters without a value are ignored by the API, so request with "types=" would return
        // all the pickup points, which is not what we want in such case.
        const ppTypes = Olza.Widget.getSelectedPickupPointTypeFiltersAsString();
        if (ppTypes.length === 0) {
            Olza.Widget.clearNearbyList();
            Olza.Widget.unlockNearbyContainer();
            OlzaSem.release(lockKey);
        } else {
            nearbyXhr.send();
        }

        Olza.Widget.setLastUsedNearbyLocation(latLng);
    },

    /**
     * Clears the nearby container. If showEmptyPlaceholder is TRUE, adds user friendly message.
     * Additionally, if distanceInMeters is not NULL, user friendly message will include that value
     * in the message.
     *
     * @param {boolean} showEmptyPlaceholder If TRUE, adds user friendly message if list is empty.
     * @param {int|null} distanceInMeters Radius in meters that was used to look for nearby PPs.
     */
    clearNearbyList: function(showEmptyPlaceholder = true, distanceInMeters = null) {
        $('#olza-nearby').empty();
        if (showEmptyPlaceholder) {
            // FIXME: hardcoded div. Should be part of layout (hidden by default).
            const labelKey = (distanceInMeters !== null) ? 'nearby-no-points-in-radius' : 'nearby-no-points-matching-filters';
            const label = Olza.Widget.translate(labelKey).replace('%s', distanceInMeters);
            $('#olza-nearby').append(`<div id="olza-closest-points-list-empty-label">${label}</div>`);
        }
    },

    /**
     * Processes /nearby response.
     *
     * @param {object} json API /nearby response data.
     * @param {object|null} excludePP Pickup Point to be excluded from the list.
     */
    processNearbyResponse: function(json, excludePP) {
        if (!json.success) {
            const msg = Olza.Widget.translate('api-error-occurred').replace('%s', json.message);
            OlzaLog.error(msg);
            return;
        }

        let ppCount = json.data.items.length;
        if (ppCount > 0 && excludePP !== null) {
            for (let i = 0; i < ppCount; i++) {
                const pp = json.data.items[i];
                if (pp.location.latitude === excludePP.location.latitude
                    && pp.location.longitude === excludePP.location.longitude) {
                    ppCount--;
                    break;
                }
            }
        }

        Olza.Widget.clearNearbyList(ppCount === 0, json.data.metadata.radius);

        for (let i = 0; i < ppCount; i++) {
            const pp = json.data.items[i];

            if (excludePP !== null) {
                if (pp.location.latitude === excludePP.location.latitude
                    && pp.location.longitude === excludePP.location.longitude) {
                    continue;
                }
            }

            const distance = pp.location.distance;
            // TODO: support imperial units maybe?
            const distanceStr = (distance > 1000)
                ? OlzaUtil.round(distance / 1000) + ' km'
                : OlzaUtil.round(distance) + ' m';

            const logoUrl = OlzaUtil.getLogoImgUrl(Olza.Widget.getOptions(), pp.spedition);
            const nameToDisplay = (pp.id !== pp.names[0]) ? `${pp.names[0]} [${pp.id}]` : pp.names[0];
            const featureIcons = Olza.Widget.getFeatureIconsAsHtml(pp);
            const address = ('address' in pp) ? pp.address.full : '';

            const html = `
                    <div class="olza-nearby-clickable" 
                            data-country="${pp.address.country}" data-spedition="${pp.spedition}" data-id="${pp.id}"
                            onClick="Olza.Widget.showDetailsClickHandler(this); return true;">
                    <div class="olza-nearby-item"><div><img class="logo" alt="Logo" src="${logoUrl}" /></div>
                    <div> 
                        <span class="olza-nearby-name">${nameToDisplay}</span><br />
                        <span class="olza-nearby-address">${address}</span><br />
                        <span class="olza-nearby-distance">${distanceStr}</span><span class="olza-nearby-feature-icons">${featureIcons}</span><br />
                    </div>
                </div>`;

            $('#olza-nearby').append(html);
        }
    },

    /** ***************************************************************************************** **/

    /**
     * Returns coordinates of Location used to fetch most recent nearby PPs or NULL if none were yet used.
     *
     * @returns {LatLng|null}
     */
    getLastUsedNearbyLocation: () => Olza.Widget._updateNearbyLastUsedLocation,

    /**
     * Stores coordinates of Location used to fetch most recent nearby PPs.
     *
     * @param {LatLng|null} location Location object to store
     */
    setLastUsedNearbyLocation: (location) => Olza.Widget._updateNearbyLastUsedLocation = location,

    /**
     * Coordinates of Location used to fetch most recent nearby PPs
     *
     * @type {LatLng|null}
     */
    _updateNearbyLastUsedLocation: null,

    /** ***************************************************************************************** **/

    /**
     * Returns dictionary of available speditions, as returned by "config/" API + user config intersection.
     * Returns empty dictionary if no available spedition matches the user requirements.
     * Returns data as [spedition1Code => {spedition1RelatedData}, ...]     *
     *
     * @returns {{}}
     */
    getAvailableSpeditions: () => Olza.Widget.getOption('availableSpeditions', {}),

    /**
     * @param {dict} speditions
     */
    setAvailableSpeditions: (speditions) => Olza.Widget.setOption('availableSpeditions', speditions),

    /** ***************************************************************************************** **/

    /**
     * Returns list of spedition codes. It is usually just one code, but for "Display All" all
     * available codes are returned.
     *
     * Must not be called before after config-internal response is processed.
     *
     * @returns {string[]}
     */
    getSelectedSpeditions: function() {
        const selectDomId = 'olza-search-spedition-select';
        const defaults = Object.keys(Olza.Widget.getAvailableSpeditions());
        return Olza.Widget.getUserChoiceFromSelectElement(selectDomId, defaults);
    },

    /**
     * Returns list of spedition codes as comma separated string elements. It is usually just one code, but for
     * "Display All" all available codes are returned.
     *
     * Must not be called before after config-internal response is processed.
     *
     * @returns {string}
     */
    getSelectedSpeditionsAsString: () => Olza.Widget.getSelectedSpeditions().join(','),

    /**
     * Returns selected pickup point type filters. It is usually just one code but
     * for "Display All" all available codes are returned.
     *
     * Must not be called before after config-internal response is processed.
     *
     * @returns {string[]}
     */
    getSelectedPickupPointTypeFilters: function() {
        const selectedTypes = [];

        const types = ['locker', 'point', 'post'];
        for (const singleType of types) {
            if ($(`#olza-search-filter-pp-type-${singleType}-state`).prop('checked')) {
                selectedTypes.push(singleType);
            }
        }
        return selectedTypes
    },

    /**
     * Returns list of selected PP type constraints as comma separated string.
     *
     * @return {string}
     */
    getSelectedPickupPointTypeFiltersAsString: () => Olza.Widget.getSelectedPickupPointTypeFilters().join(','),

    /**
     * Returns values of selected options of given <SELECT> an element.
     *
     * Must not be called before after config-internal response is processed.
     *
     * @param {string} selectDomId DOM ID of `<SELECT>` element to read.
     * @param {string[]} defaults List of string to be returned if no valid option is selected.
     *
     * @returns {string[]}
     */
    getUserChoiceFromSelectElement: function(selectDomId, defaults) {
        const domId = `#${selectDomId} option:selected`;
        const selectVal = $(domId).val();
        // The "All" select element got empty value so it will fall here too
        if ((typeof selectVal === 'undefined') || (selectVal === null) || (selectVal === '')) {
            return defaults;
        }
        return [selectVal];
    },

    /** ***************************************************************************************** **/

    /**
     * Returns list of all the icons of the features worth announcing. Returns an empty list if there's none.
     * Each list element is an object with "file" final file to be used with HTML, and "label" being human-     * readable label corresponding to the icon.
     *
     * @param {object} pp Pickup Point to inspect for all the features.
     *
     * @return {object[]}
     */
    getFeatureIcons: function(pp) {
        /** object[] */
        const icons = [];

        // TODO: nearby will not show 24/7 as hours are not available for fetching there
        if ('hours' in pp) {
            if (pp.hours.open247) {
                const iconLabel = Olza.Widget.translate('open247');
                const iconFileName = OlzaUtil.getServiceIconFileName('open247');
                icons.push({file: iconFileName, label: iconLabel});
            }
        }

        // Additional service icons
        if ('services' in pp) {
            if ('cod' in pp.services) {
                // No COD payment icons if no COD is available
                if (pp.services.cod.available) {

                    // COD availability icon
                    const codIconLabel = Olza.Widget.translate('cod-available');
                    const codIconFileName = OlzaUtil.getServiceIconFileName('cod');
                    icons.push({file: codIconFileName, label: codIconLabel});

                    // COD payment types
                    for (const paymentType of pp.services.cod.payments) {
                        const iconLabel = Olza.Widget.translate(`payment-type-${paymentType}`);
                        const iconFileName = OlzaUtil.getPaymentTypeIconFileName(paymentType);
                        icons.push({file: iconFileName, label: iconLabel});
                    }
                }
            }
        }

        return icons;
    },

    /**
     * @param {object} pp Pickup Point to inspect for all the features.
     */
    getFeatureIconsAsHtml: function(pp) {
        let html = '';
        const featureIcons = Olza.Widget.getFeatureIcons(pp);
        for (const icon of featureIcons) {
            html += `<img class="icon" src="${img(icon.file)}" alt="${icon.label}" title="${icon.label}" />`;
        }
        return html;
    },

    /**
     * Injects HTML markup into targetDivId with all the feature icons represented. Note, will always clear
     * target div prior to inserting anything, Id/class of a target element for the icon markup to be injected info.
     *
     * @param {object} pp Pickup Point to inspect for all the features.
     * @param {string} targetDivId Id/class of target element for the icons markup to be injected info.
     */
    getFeatureIconsAndInject: function(pp, targetDivId) {
        const html = Olza.Widget.getFeatureIconsAsHtml(pp);
        $(`#${targetDivId}`).empty().html(html);
    },

    /**
     * Shows widget overlay.
     *
     * @returns {boolean} Returns TRUE (unconditionally).
     */
    open: function() {
        $('#olza-widget-main').show();
        return true;
    },

    /**
     * When in modal mode, hides widget overlay (save to call even if widget is not visible).
     * Does nothing when called for widget in embedded mode.
     *
     * @returns {boolean} Returns TRUE (unconditionally).
     */
    close: function() {
        if (Olza.Widget.isModal()) {
            $('#olza-widget-main').hide();
        }
        return true;
    },

    /**
     * Refreshes nearby list based on current state of filters
     *
     * NOTE: also used by other event handlers!
     */
    refineNearby: function() {
        const pp = Olza.Widget.getSelectedPickupPoint();
        const lastNearbyLocation = (pp === null)
            ? Olza.Widget.getLastUsedNearbyLocation()
            : L.latLng(pp.location.latitude, pp.location.longitude);

        // If no ppType filter checkbox is selected, then we do not want to do any API searches, because
        // optional filters without a value are ignored by the API, so request with "types=" would return
        // all the pickup points, which is not what we want in such case.
        const ppTypeFilters = Olza.Widget.getSelectedPickupPointTypeFilters();
        if (ppTypeFilters.length > 0) {
            if (lastNearbyLocation !== null) {
                Olza.Widget.updateNearby(lastNearbyLocation, pp);
            }

            const speditions = Olza.Widget.getSelectedSpeditions();
            const search = '';
            const services = Olza.Widget.getServiceFilters();
            const payments = Olza.Widget.getPaymentTypeFilters();
            const mapBounds = null;
            const triggerUpdateOfNearbys = false;
            const adjustMapToFit = false;

            Olza.Widget.doApiSearchQueryArgs(speditions, search, services, payments, mapBounds, ppTypeFilters,
                triggerUpdateOfNearbys, adjustMapToFit);
        } else {
            Olza.Widget.clearMapAndNearby();
        }

        return true;
    },

    /**
     * Removes all shown PP markers and clears nearby list.
     *
     * @param lockContainers If TRUE, will lock map containers while clearing (default).
     */
    clearMapAndNearby: function(lockContainers = true) {
        if (lockContainers) {
            Olza.Widget.lockMapContainer();
        }

        Olza.Widget.markers.clearLayers();
        Olza.Widget.closeDetailsPane();
        Olza.Widget.clearNearbyList();

        if (lockContainers) {
            Olza.Widget.unlockMapContainer();
        }
    },

    /**
     * Returns list of requested supported service type constraints.
     *
     * @return {string[]}
     */
    getServiceFilters: function() {
        const services = [];
        if ($('#olza-search-filter-service-cod-state').prop('checked')) {
            services.push('cod');
        }
        return services;
    },

    /**
     * Returns list of requested supported service type constraints as comma separated string.
     *
     * @return {string}
     */
    getServiceFiltersAsString: () => Olza.Widget.getServiceFilters().join(','),

    /**
     * Returns list of selected payment type constraints.
     *
     * @return {string[]}
     */
    getPaymentTypeFilters: function() {
        const payments = [];
        if ($('#olza-search-filter-payment-cash-state').prop('checked')) {
            payments.push('cash');
        }
        if ($('#olza-search-filter-payment-card-state').prop('checked')) {
            payments.push('card');
        }
        if ($('#olza-search-filter-payment-online-state').prop('checked')) {
            payments.push('online');
        }
        return payments;
    },

    /**
     * Returns list of selected payment type constraints as comma separated string.
     *
     * @return {string}
     */
    getPaymentTypeFiltersAsString: () => Olza.Widget.getPaymentTypeFilters().join(','),


    /**
     * Shortcut to doApiSearchQueryArgs() that collects all the args from UI elements and then triggers API call
     * and response processing.
     *
     * @param {boolean} triggerUpdateOfNearbys If TRUE, will trigger update of nearby PPs.
     * @param {boolean} useMapBounds If TRUE, will use maps' current visible area bounds to narrow the search.
     * @param {function|null} callback Callback to be called after nearby pickup points are fetched.
     */
    doApiSearchQueryFromUi: async function(triggerUpdateOfNearbys, useMapBounds = false, callback = null) {
        // const search = $("#olza-map-search-input").val();
        const search = '';
        const speditions = Olza.Widget.getSelectedSpeditions();
        const services = Olza.Widget.getServiceFilters();
        const payments = Olza.Widget.getPaymentTypeFilters();
        const mapBounds = useMapBounds ? Olza.Widget.map.getBounds() : null;
        const ppTypes = Olza.Widget.getSelectedPickupPointTypeFilters();
        const adjustMapToFit = false;

        // Draws clipping bounds
        if (mapBounds !== null && Olza.Widget.getDebugOption('debug.drawMapBounds', false)) {
            L.rectangle(mapBounds, {color: 'blue', weight: 1}).addTo(Olza.Widget.mapDebugLayer);
        }

        Olza.Widget.doApiSearchQueryArgs(speditions, search, services, payments, mapBounds, ppTypes,
            triggerUpdateOfNearbys, adjustMapToFit, callback);
    },

    /** Number of API /find queries successfully completed. **/
    apiFindQueryCompletedCount: 0,

    initFindApiQueryCounters: function() {
        Olza.Widget.apiFindQueryCompletedCount = 0;
    },

    /**
     * Initializes handling of "/search" API query.
     *
     * @param {string[]} speditions Comma-separated list of speditions to be included in search results.
     * @param {string} search Search string. Empty string allowed.
     * @param {string[]} services List of services to be included in search results.
     * @param {string[]} payments List of payments to be included in search results.
     * @param {LatLngBounds|null} mapBounds Optional map area bounds for geo-filtering.
     * @param {string[]|null} ppTypesFilter Optional list of pickup point types to be included in search results.
     * @param {boolean} triggerUpdateOfNearbys If TRUE, will trigger update of nearbys PP (default).
     * @param {boolean} adjustMapToFit It TRUE (default), will adjust map viewport and zoom to show all loaded PPs.
     * @param {function|null} callback Callback to be called after nearby pickup points are fetched.
     */
    doApiSearchQueryArgs: function(speditions,
                                   search = '',
                                   services = [],
                                   payments = [],
                                   mapBounds = null,
                                   ppTypesFilter = null,
                                   triggerUpdateOfNearbys = true,
                                   adjustMapToFit = true,
                                   callback = null) {
        const findXhr = Olza.Widget.getApiFindXhr(speditions, search, services, payments, mapBounds, ppTypesFilter);
        findXhr.onload = function() {
            Olza.Widget.processFindApiResponse(this, adjustMapToFit);

            // Trigger nearby list update based on current map bounding box.
            if (triggerUpdateOfNearbys) {
                let nearbyCenter = Olza.Widget.getLastUsedNearbyLocation();
                if (nearbyCenter === null) {
                    nearbyCenter = Olza.Widget.map.getCenter();
                }
                Olza.Widget.updateNearby(nearbyCenter, null, callback);
            }

            // updateNearby will set own lock on nearby pane, so we can remove all
            // our previously set locks w/o any visual glitches/blinks/flashes.
            Olza.Widget.unlockWidget();

            Olza.Widget.apiFindQueryCompletedCount++;
        }
        findXhr.onerror = function() {
            OlzaLog.error(Olza.Widget.translate('find-request-failed'));
            Olza.Widget.unlockWidget();
        }

        Olza.Widget.markers.clearLayers();
        Olza.Widget.closeDetailsPane();
        Olza.Widget.lockWidget();
        findXhr.send();
    },

    /**
     * Sends "/search" API query and processes the response or errors.
     *
     * @param {string[]} speditions List of speditions to be included in search results.
     * @param {string} search Search string.
     * @param {string[]} services List of services to be included in search results.
     * @param {string[]} payments List of payments to be included in search results.
     * @param {LatLngBounds|null} mapBounds Optional map bounds for geo-filtering.
     * @param {string[]|null} ppTypeFilter Optional list of pickup point types to be included in search results.
     */
    getApiFindXhr: function(speditions, search = '', services = [], payments = [], mapBounds = null, ppTypeFilter = null) {
        let params = {
            country: Olza.Widget.getOption('api.country'),
            spedition: speditions.join(','),
        };

        if (services.length > 0) {
            params.services = services.join(',')
        }

        if (payments.length > 0) {
            params.payments = payments.join(',')
        }

        if (mapBounds !== null) {
            const upperLeft = mapBounds.getNorthWest()
            const lowerRight = mapBounds.getSouthEast()
            params.bounds = [
                upperLeft.lat, upperLeft.lng,
                lowerRight.lat, lowerRight.lng
            ].join(',');
        }

        if (ppTypeFilter !== null) {
            params.types = ppTypeFilter.join(',')
        }

        search = search || '';
        if (search.trim() !== '') {
            params.search = search
        }

        params.widgetVersion = Olza.Widget.getVersionString()

        return Olza.Widget.getXhr('find', params)
    },

    /**
     * Processes API search response, by decoding provided JSON, planting all PPs on the map,
     * adjusting map bounds to fit all loaded PPs etc.
     *
     * @param {XMLHttpRequest} xhr
     * @param {boolean} adjustMapToFit It TRUE (default), will adjust map viewport and zoom to show all loaded PPs.
     * @param {boolean} uiLocking If TRUE (default), will lock UI during processing.
     */
    processFindApiResponse: function(xhr, adjustMapToFit = true, uiLocking = true) {
        if (xhr.responseText.trim() === '') {
            return;
        }

        let json = JSON.parse(xhr.responseText);
        if (!json.success) {
            switch (json.code) {
                case 100: // Object not found
                    OlzaLog.debug('Pickup points not found for specified combination.');
                    break;
                default:
                    const msg = Olza.Widget.translate('find-request-processing-failed').replace('%s', json.message);
                    OlzaLog.error(msg);
            }
            return;
        }

        Olza.Widget.markers.clearLayers();
        if (json.data.items.length === 0) {
            return;
        }

        if (uiLocking) {
            Olza.Widget.lockMapContainer();
        }

        let topLeftLat = null;
        let topLeftLng = null;
        let bottomRightLat = null;
        let bottomRightLng = null;

        for (let i = 0; i < json.data.items.length; i++) {
            const pp = json.data.items[i];
            if (pp.location == null) continue;

            const ppLat = pp.location.latitude;
            const ppLng = pp.location.longitude;

            let speditionCode = pp.spedition.toLowerCase();

            if (!(speditionCode in Olza.Widget.iconsCache)) {
                const pinImgUrl = OlzaUtil.getPinImgUrl(Olza.Widget.getOptions(), speditionCode);
                const icon = L.icon({
                    iconUrl: pinImgUrl,
                    iconSize: [31, 50], // size of the pin image
                    iconAnchor: [15, 50], // point of the icon which will correspond to marker's location
                    popupAnchor: [0, -56] // point from which the popup should open relative to the iconAnchor
                });
                Olza.Widget.iconsCache[speditionCode] = icon;
            }

            if (topLeftLat != null) {
                if (topLeftLat > ppLat) topLeftLat = ppLat;
                if (topLeftLng > ppLng) topLeftLng = ppLng;
                if (bottomRightLat < ppLat) bottomRightLat = ppLat;
                if (bottomRightLng < ppLng) bottomRightLng = ppLng;
            } else {
                topLeftLat = pp.location.latitude;
                topLeftLng = pp.location.longitude;
                bottomRightLat = pp.location.latitude;
                bottomRightLng = pp.location.longitude;
            }

            const speditionName = Olza.Widget.translateSpeditionName(pp.spedition);
            const markerTitle = `${pp.names[0]} (${speditionName})<br />${pp.address.full}`;
            const marker = L.marker(L.latLng(ppLat, ppLng), {
                title: markerTitle,
                icon: Olza.Widget.iconsCache[speditionCode]
            });
            // Handle direct click on the marker to show details of clicked PP.
            marker.on('click', (_event) => Olza.Widget.showDetails(pp.address.country, pp.spedition, pp.id));
            marker.bindPopup(markerTitle);
            Olza.Widget.markers.addLayer(marker);
        }
        Olza.Widget.map.addLayer(Olza.Widget.markers);

        // dataset must contain at least two points unless we check using just one to
        // calculate bounding box still makes sense.
        if (adjustMapToFit && json.data.items.length >= 2) {
            const p1 = L.latLng(topLeftLat, topLeftLng);
            const p2 = L.latLng(bottomRightLat, bottomRightLng);
            Olza.Widget.map.fitBounds(L.latLngBounds(p1, p2));
        }

        if (uiLocking) {
            Olza.Widget.unlockMapContainer();
        }
    },

    /**
     * This method concludes the PP picker and invoked once a user approves his/her selection.
     * It will close the picker and call user provided callback function to hand details of selected PP.
     *
     * @returns {boolean}
     */
    pickThisPoint: function() {
        const item = Olza.Widget.getSelectedPickupPoint();
        if (item != null) {
            Olza.Widget.close();

            // FIXME: Callbacks should also be readable via getOption() method.
            Olza.Widget.getOptions().callbacks.onSelect(item);
        }

        return true;
    },

    /**
     * Helper methods that invokes showDetails() using PP details collected from data-* attributes
     * of the element that triggered the action. Usued as click handler for nearby list's elements.
     *
     * @param {Event} element The event object associated with the click.
     */
    showDetailsClickHandler: async function(element) {
        const countryCode = element.getAttribute('data-country')
        const spedition = element.getAttribute('data-spedition')
        const pickupPointId = element.getAttribute('data-id')

        await Olza.Widget.showDetails(countryCode, spedition, pickupPointId)
    },

    /**
     * Prepares to show PP details. It validates provided arguments and then tries to obtain PP details
     * from Cache. If not PP is found in cache, will trigger details download from API.
     *
     * @param {string} countryCode Country code of the PP.
     * @param {string} speditionCode Code of PP parent spedition.
     * @param {string} pickupPointId ID of the PP to get details of
     */
    showDetails: async function(countryCode, speditionCode, pickupPointId) {
        if (typeof countryCode === "undefined" || countryCode === '') {
            OlzaLog.logError('Argument missing or empty: country');
            return
        }
        if (typeof speditionCode === "undefined" || speditionCode === '') {
            OlzaLog.logError('Argument is missing or empty: spedition');
            return
        }
        if (typeof pickupPointId === "undefined" || pickupPointId === '') {
            OlzaLog.logError('Argument missing or empty: pickupPointId');
            return
        }

        // Note, locking here is important as DetailsPane will try to unlock itself.
        Olza.Widget.lockDetailsContainer();

        const pp = OlzaCache.get(countryCode, speditionCode, pickupPointId);
        if (pp !== null) {
            Olza.Widget.selectAndShowDetails(pp);
        } else {
            Olza.Widget.clearDetailsPane();
            Olza.Widget.showDetailsPane();

            Olza.Widget.downloadAndShowPpDetails(countryCode, speditionCode, pickupPointId);
        }
    },

    /**
     * Download details of the pickup point. Once data is successfully fetched,
     * selectAndShowDetails() is invoked to present all the details.
     *
     * @param {string} country Code of Pickup Point country.
     * @param {string} spedition Code of Pickup Point parent spedition.
     * @param {string} pickupPointId Unique ID of the Pickup Point.
     */
    downloadAndShowPpDetails: function(country, spedition, pickupPointId) {
        const detailsXhr = Olza.Widget.getXhr('details', {
            widgetVersion: Olza.Widget.getVersionString(),
            country: country,
            spedition: spedition,
            id: pickupPointId,
        });
        detailsXhr.onload = function() {
            const json = JSON.parse(detailsXhr.responseText);
            if (!json.success) {
                Olza.Widget.closeDetailsPane();
                const msg = Olza.Widget.translate('api-error-occurred').replace('%s', json.message);
                OlzaLog.error(msg);
                return;
            }
            const pp = json.data.items[0];
            Olza.Widget.selectAndShowDetails(pp);
        }
        detailsXhr.onerror = function() {
            Olza.Widget.closeDetailsPane();
            const msg = Olza.Widget.translate('details-download-failed');
            OlzaLog.error(msg);
        }

        detailsXhr.send();
    },

    /**
     * Updates PP details pane with provided instance.
     *
     * NOTE: it assumes that UI lock on its container it already set and will try to unlock it once all data is set up.
     *
     * @param {?object} pp
     */
    selectAndShowDetails: function(pp) {
        Olza.Widget.setSelectedPickupPoint(pp);
        if (pp === null) return;

        // Spedition logo
        const logoUrl = OlzaUtil.getLogoImgUrl(Olza.Widget.getOptions(), pp.spedition);
        $('#olza-pp-details-logo').attr('src', logoUrl);

        // Spedition details
        const speditionName = Olza.Widget.translateSpeditionName(pp.spedition);
        const speditionDetails = `<strong>${pp.id} (${speditionName})</strong><br />${pp.address.full}<br />`;
        $('#olza-pp-details-info').html(speditionDetails);

        // Building opening hours markdown and icons
        $('#olza-pp-details-hours').empty();
        if (!pp.hours.open247) {
            let hoursMarkdown = '';

            // NOTE: Day names are array keys, do not translate/rename.
            const days = ['monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday', 'sunday'];
            for (const dayKey of days) {
                if (pp.hours[dayKey] != null) {
                    const openTime = pp.hours[dayKey].hours;
                    const breakTime = pp.hours[dayKey].break;
                    if (openTime != null) {
                        hoursMarkdown += `<tr><td id="olza-pp-details-day-${dayKey}">${Olza.Widget.translate(dayKey)}</td><td>${openTime}`;
                        if (breakTime != null) hoursMarkdown += ` (${breakTime})`;
                        hoursMarkdown += '</td></tr>';
                    }
                }
            }

            if (hoursMarkdown !== '') {
                const tableTitle = Olza.Widget.translate('open-hours');
                const html = `
                    <table>
                        <tr><td colspan="2">
                            <span id="olza-pp-details-hours-header">${tableTitle}</span>
                        </td></tr>
                        ${hoursMarkdown}
                    </table>`;
                $('#olza-pp-details-hours').html(html);
            }
        }

        Olza.Widget.getFeatureIconsAndInject(pp, 'olza-pp-details-service-icons');

        Olza.Widget.showSelectedPointOnMap();
        Olza.Widget.showDetailsPane();

        Olza.Widget.unlockDetailsContainer();

        const nearbyLoc = L.latLng(pp.location.latitude, pp.location.longitude);
        Olza.Widget.updateNearby(nearbyLoc, pp);
    },

    /**
     * Clears (zeroes) PP details pane fields. Mostly used to prepare details pane to be shown
     * while downloading PP details, so it's better to make it empty than show details of
     * previously selected PP.
     */
    clearDetailsPane: function() {
        $('#olza-pp-details-info').empty();
        $('#olza-pp-details-hours').empty();
        $('#olza-pp-details-service-icons').empty();

        const blankLogo = img('./empty-logo.svg');
        $('#olza-pp-details-logo').attr('src', blankLogo);
    },

    /**
     * This method is called when a user clicks "Show on Map" button in PP details pane and its main
     * role is to move a map to selected PP location.
     */
    showSelectedPointOnMap: function() {
        const pp = Olza.Widget.getSelectedPickupPoint();
        if (pp != null) {
            const zoom = Olza.Widget.getOption('ui.detailsZoom', 17);
            Olza.Widget.map.flyTo(L.latLng(pp.location.latitude, pp.location.longitude), zoom);
        } else {
            OlzaLog.warn('No Pickup Point marked selected.');
        }
        return true;
    },

    /**
     * Sets Widget language and triggers UI refresh to reflect the change.
     *
     * @param {string|null} language Language code to set as widget's main language.
     */
    setLanguage: function(language) {
        if (language === null) {
            OlzaLog.warn('Cannot set language: null');
            return;
        }

        Olza.Widget.setOption('ui.language', language);
        Olza.Widget.updateLabels();
        Olza.Widget.populateSpeditionSelect(false);
        Olza.Widget.updateLanguageSelector(language);

        return true;
    },

    /**
     * Opens pickup point details pane. Note it only changes pane visibility and does not populate
     * details view with any data. Safe to call this method even if the details pane is already open.
     *
     * @returns {boolean}
     */
    showDetailsPane: function() {
        $('#olza-pp-details').show();
        return true;
    },
    /**
     * Closes pickup point details pane if opened. Safe to call on closed pane.
     *
     * @returns {boolean}
     */
    closeDetailsPane: function() {
        $('#olza-pp-details').hide();
        return true;
    },

    // ------[ Moved Olza.Ui ]-----

    /**
     * Initializes language selection flag-buttons based on a built-in translation set.
     *
     * @param {string} currentLang Language code to mark as selected.
     */
    initLanguageSelector: function(currentLang) {
        let html = '';
        for (const langCode of OlzaLang.getAllTranslationsLanguageCodes()) {
            const flagLanguageCode = Olza.Widget.mapFlagCode(langCode);
            const flagImage = img(`./flag-${flagLanguageCode}.svg`);
            const langName = OlzaLang.getByLang(langCode, 'native-language-name');
            html += `<li>
                        <span class="dropdown-item" onClick="Olza.Widget.setLanguage('${langCode}'); return true;">
                            <img alt="${langName}" src="${flagImage}"> ${langName}
                        </span>
                     </li>`;
        }
        $('#olza-language-selector-languages').empty().append(html);
        Olza.Widget.updateLanguageSelector(currentLang);
    },

    /**
     * Sets current language indicator to specified language.
     *
     * @param {string} langCode
     */
    updateLanguageSelector: function(langCode) {
        const langName = OlzaLang.getByLang(langCode, 'native-language-name');
        const flagCode = Olza.Widget.mapFlagCode(langCode);

        const html = `<img alt="${langName}" src="https://flagcdn.com/w80/${flagCode}.png"> ${langName}`;
        $('#olza-language-selector-current').empty().append(html);
    },

    /**
     * Updates UI language to reflect currently selected language.
     */
    updateLabels: function() {
        $('#olza-widget-title').text(Olza.Widget.translate('title'));

        $('#olza-map-search-input').attr('placeholder', Olza.Widget.translate('search-input-placeholder'));

        let detailPaneButtons = {
            'olza-pp-details-show-on-map-button-label': 'details-show-on-map-button',
            'olza-pp-details-close-button-label': 'details-hide-details-button',
            'olza-pp-details-pick-button-label': 'details-select-button',
        };
        for (const buttonLabelSpanId of Object.keys(detailPaneButtons)) {
            const labelKey = detailPaneButtons[buttonLabelSpanId];
            $(`#${buttonLabelSpanId}`).text(Olza.Widget.translate(labelKey));
        }

        $('#olza-pp-closest-point-label').text(Olza.Widget.translate('closest-points-title'));

        $('#olza-pp-details-hours-header').html(Olza.Widget.translate('open-hours'));
        const days = ['monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday', 'sunday'];
        for (const day of days) {
            const dayKey = `olza-pp-details-day-${day}`;
            $(`#${dayKey}`).text(Olza.Widget.translate(day));
        }

        // PP Type filter labels
        const ppTypes = ['locker', 'point', 'post'];
        for (const ppType of ppTypes) {
            const labelId = `olza-search-filter-pp-type-${ppType}-label`;
            const strKey = `filter-pp-type-${ppType}`;
            $(`#${labelId}`).text(Olza.Widget.translate(strKey));
        }

        $('#olza-search-toggle-additional-filters-button').text(Olza.Widget.translate('advanced-filters-group-label'));
    },

    /**
     * Populates spedition select widget.
     *
     * @param {boolean} triggerChangeEvent
     */
    populateSpeditionSelect: function(triggerChangeEvent = true) {
        Olza.Widget.disableOnSpeditionChangeListener();

        const selectId = 'olza-search-spedition-select';

        /** @type {string|null} */
        let selectedSpedition = OlzaUtil.getSelectedElementValue(selectId);

        const select = $(`#${selectId}`);
        select.empty();
        const labelAll = Olza.Widget.translate('spedition-selector-all');
        const allOptionSelected = selectedSpedition === null ? 'selected="selected"' : '';
        select.append(`<option value="" ${allOptionSelected}>${labelAll}</option>`);
        const speditionKeys = Object.keys(Olza.Widget.getAvailableSpeditions());
        for (const key of speditionKeys) {
            const translatedLabel = Olza.Widget.translateSpeditionName(key);
            select.append(`<option value="${key}">${translatedLabel}</option>`);
        }

        select.val(selectedSpedition)
        Olza.Widget.enableOnSpeditionChangeListener();

        if (triggerChangeEvent) {
            select.change()
        }
    },

    /**
     * Activates all "busy" indicators.
     */
    lockWidget: function() {
        Olza.Widget.lockSearchBox();
        Olza.Widget.lockMapContainer();
        Olza.Widget.lockNearbyContainer();
    },
    /**
     * Tries to deactivate all "busy" indicators.
     */
    unlockWidget: function() {
        Olza.Widget.unlockNearbyContainer();
        Olza.Widget.unlockMapContainer();
        Olza.Widget.unlockSearchBox();
    },

    /**
     * Activates "busy" indicator on map container.
     */
    lockMapContainer: () => Olza.Widget.setUiLock('olza-map-busy'),
    /**
     * Tries to deactivate "busy" indicator on map container.
     */
    unlockMapContainer: () => Olza.Widget.releaseUiLock('olza-map-busy'),

    /**
     * Activates "busy" indicator on right pane (nearby container).
     */
    lockNearbyContainer: () => Olza.Widget.setUiLock('olza-nearby-busy'),
    /**
     * Tries to deactivate "busy" indicator on right pane (nearby container).
     */
    unlockNearbyContainer: () => Olza.Widget.releaseUiLock('olza-nearby-busy'),

    /**
     * Tries to activate "busy" indicator on PP details pane
     */
    lockDetailsContainer: () => Olza.Widget.setUiLock('olza-pp-details-busy'),

    /**
     * Tries to release PP details pane "busy" lock and indicator.
     */
    unlockDetailsContainer: () => Olza.Widget.releaseUiLock('olza-pp-details-busy'),

    /**
     * Tries to activate "busy" indicator on search box
     */
    lockSearchBox: () => Olza.Widget.setUiLock('olza-search-busy'),

    /**
     * Tries to release search box's "busy" lock and indicator
     */
    unlockSearchBox: () => Olza.Widget.releaseUiLock('olza-search-busy'),

    /**
     * Map holding lock count for each reference divIds.
     *
     * @type {string, int}
     */
    _lockCounters: {},
    /**
     * Increases lock count for given divId by one.
     * Shows "busy" indicator overlay if not yet visible.
     *
     * @param divId ID of the DIV that is root for "busy" indicator.
     */
    setUiLock: function(divId) {
        let counter = 0;
        if (divId in Olza.Widget._lockCounters) {
            counter = Olza.Widget._lockCounters[divId];
        }
        counter++;
        Olza.Widget._lockCounters[divId] = counter;

        if (counter === 1) {
            $(`#${divId}`).show();
        }
    },

    /**
     * Decreases lock count for given divId by one.
     * If lock count reaches zero, hides "busy" indicator overlay.
     *
     * @param divId ID of the DIV that is root for "busy" indicator.
     */
    releaseUiLock: function(divId) {
        if (!(divId in Olza.Widget._lockCounters)) {
            OlzaLog.debug(`Unknown lock key: ${divId}`);
            return;
        }
        let counter = Olza.Widget._lockCounters[divId] - 1;
        Olza.Widget._lockCounters[divId] = counter;

        if (counter === 0) {
            $(`#${divId}`).hide();
            delete Olza.Widget._lockCounters[divId];
        }
    },

    /**
     * Attempts to get browser current location and then, if that was successful, pan the map to show
     * that location. Note that for this method to work, user browser must support it and also have the feature
     * enabled so pages can obtain this information. If for any reason location cannot be obtained, this method
     * does nothing (but logs the error).
     */
    showCurentBrowserLocation: function() {
        if ('geolocation' in navigator) {
            navigator.geolocation.getCurrentPosition(function(position) {
                const latLng = L.latLng(position.coords.latitude, position.coords.longitude);
                Olza.Log.debug(`Got your browser position: Lat: ${position.coords.latitude}, Lng: ${position.coords.longitude}`);
                Olza.Widget.map.panTo(latLng);
                Olza.Widget.updateNearby(latLng);
            });
        } else {
            Olza.Log.debug('Geolocation in browser is disabled or not available.');
        }
    },

    /**
     * Returns current version of the widget
     *
     * @returns {string}
     */
    getVersionString: () => packageJson.version,

    /** ***************************************************************************************** **/

};

/** ********************************************************************************************* **/

export let Widget = Olza.Widget
