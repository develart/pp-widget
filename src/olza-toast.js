/*
 * Olza Logistic's Pickup Points selector Widget
 *
 * @author    Marcin Orlowski <marcin.orlowski (#) develart (.) cz>
 * @copyright 2022-2025 DevelArt
 * @license   http://www.opensource.org/licenses/mit-license.php MIT
 * @link      https://bitbucket.org/develart/pp-widget/
 */

import $ from "jquery-slim";

import './plugins/toaster/toaster.js'
import './plugins/toaster/toaster.css'

/** ********************************************************************************************* **/

const OlzaToast = {

    /**
     * https://github.com/rosshatokay/toasterjs/
     *
     * @param {string} msg
     * @param {int} timeout Timeout after which toast is hidden (in millis). Set to 0 (zero) for no timeout.
     */
    show: function(msg, timeout = 1000 * 20) {
        $.toast({
            content: msg,
            hideAfter: timeout,
            position: 'bottom-right',
        })
    },
}

/** ********************************************************************************************* **/

export default OlzaToast
