/*
 * Olza Logistic's Pickup Points selector Widget
 *
 * @author    Marcin Orlowski <marcin.orlowski (#) develart (.) cz>
 * @copyright 2022-2025 DevelArt
 * @license   http://www.opensource.org/licenses/mit-license.php MIT
 * @link      https://bitbucket.org/develart/pp-widget/
 */

import OlzaLangTranslations from "./translations";
import OlzaUtil from "./olza-util";

/** ********************************************************************************************* **/

const OlzaLang = {
    /**
     * Translation map.
     *
     * @type {object}
     */
    translations: OlzaLangTranslations,

    getTranslations: () => OlzaLang.translations,

    /**
     * Gets the translation associated with the given key or the key itself as string if no translation is found.
     * Note that it will try to first fall back to default language before using key as return value.
     *
     * @param {object} widgetConfig Instance of a widget configuration object.
     * @param {string} key String key to get a localized version of.
     *
     * @returns {string}
     */
    get: function (widgetConfig, key) {
        /** @type {string|null} */
        let result = null;
        if (typeof widgetConfig != 'undefined' && widgetConfig !== null) {
            const langCode = OlzaUtil.dictGet(widgetConfig, 'ui.language');
            result = OlzaLang.getByLang(langCode, key);
        }

        if (result === null) {
            result = OlzaLang.getByLang(OlzaLang.defaultLanguage, key);
        }
        if (result === null) {
            result = key;
        }
        return result;
    },
    /**
     * Returns localized version of string referenced by key or null
     * if key is unknown.
     *
     * @param {string} languageCode Language code to use.
     * @param {string} key String key to get a localized version of.
     *
     * @returns {null|string}
     */
    getByLang: function (languageCode, key) {
        let result = null;
        if (languageCode in this.translations) {
            if (key in this.translations[languageCode]) {
                result = this.translations[languageCode][key];
            }
        }
        return result;
    },

    /**
     * Default language code to fall back to in case we failed to obtain
     * anything usable from the browser. MUST be set to valid string.
     *
     * @type {string}
     */
    defaultLanguage: 'en',

    /**
     * Returns language code to use for localization.
     *
     * @returns {string}
     */
    getDefaultLanguage: () => OlzaLang.defaultLanguage,

    /**
     * Returns current language selected as user's browser's default.
     *
     * @returns {string}
     */
    getBrowserLanguage: function () {
        let browserLanguage = OlzaLang.defaultLanguage;
        for (const lang of OlzaLang.getBrowserSupportedLocales()) {
            if (lang in OlzaLang.getTranslations()) {
                browserLanguage = lang;
                break;
            }
        }
        return browserLanguage;
    },

    /**
     * Returns code of language user selected as browser's primary language.
     *
     * @param options
     * @returns {string[]|undefined}
     */
    getBrowserSupportedLocales: function (options = {}) {
        const defaultOptions = {
            languageCodeOnly: false,
        };
        const opt = {
            ...defaultOptions,
            ...options,
        };
        const browserLocales =
            typeof navigator.languages === "undefined"
                ? [navigator.language]
                : navigator.languages;
        if (!browserLocales) {
            return undefined;
        }
        return browserLocales.map(locale => {
            const trimmedLocale = locale.trim();
            return opt.languageCodeOnly
                ? trimmedLocale.split(/-|_/)[0]
                : trimmedLocale;
        });
    },

    /**
     * @returns {string[]}
     */
    getAllTranslationsLanguageCodes: () => Object.keys(OlzaLang.getTranslations()),

    /**
     * Determines if given language code is supported by the current widget configuration.
     *
     * @param {string|null} languageCode Language code to check.
     * @returns {boolean}
     */
    isLanguageSupported(languageCode) {
        const allCodes = OlzaLang.getAllTranslationsLanguageCodes();
        return allCodes.indexOf(languageCode) !== -1;
    },

    /** ***************************************************************************************** **/

}

/** ********************************************************************************************* **/

export default OlzaLang
