/*
 * Olza Logistic's Pickup Points selector Widget
 *
 * @author    Marcin Orlowski <marcin.orlowski (#) develart (.) cz>
 * @copyright 2022-2025 DevelArt
 * @license   http://www.opensource.org/licenses/mit-license.php MIT
 * @link      https://bitbucket.org/develart/pp-widget/
 */

import $ from "jquery-slim";
import OlzaLog from './olza-log.js'
import OlzaUtil from "./olza-util";
import OlzaWidgetDefaultConfig from "./config.js";

/** ********************************************************************************************* **/

/**
 * Widget configuration and related methods
 */
const OlzaConfig = {
    /**
     * Returns reference to default widget configuration object.
     *
     * @returns {object}
     */
    getDefaults: function() {
        return {...OlzaConfig.defaults};
    },

    /**
     * Default widget configuration.
     *
     * @type {object}
     */
    defaults: OlzaWidgetDefaultConfig,

    /** ***************************************************************************************** **/

    /**
     * Merges user config with widget defaults.
     *
     * First, this method tries to obtain data-* attributes from the root container, then it merges
     * user config with data-* parameters (see description of "codeFirst" config option for more).
     * then merges combined configuration into widget defaults to override.
     *
     * @param {boolean} isModal TRUE to initialize widget to run in modal mode, FALSE for emedded mode.
     * @param {object} userConfigSrc User provided configuration object.
     * @param {object|null} clickedElement Object that was triggering this action. Used only to read data-* attributes.
     *
     * @returns {object}
     */
    init: function(isModal, userConfigSrc, clickedElement = null) {
        if (userConfigSrc !== null && typeof userConfigSrc !== 'object') {
            OlzaLog.permanentError('Invalid user configuration object');
            throw 'userConfig must be a valid dictionary.';
        }

        // let's be nice and allow set "api.speditions" not only passed as list, but also a comma
        // separated string also for code provided config.
        const userSpeds = OlzaUtil.dictGet(userConfigSrc, 'api.speditions', null);
        if (typeof userSpeds === 'string') {
            OlzaUtil.dictSet(userConfigSrc, 'api.speditions', userSpeds.split(','));
        }

        // We only care about the elements we have in our built-in default config
        // Anything else is a junk and will be filtered out.
        const defaultConfigDotPaths = OlzaUtil.dictToDotPaths(OlzaConfig.getDefaults());
        const userConfigFiltered = OlzaUtil.sanitizeDict(userConfigSrc, defaultConfigDotPaths);

        // Optionally log used config
        OlzaLog.logConfig('userConfig', userConfigFiltered);

        // Let's get divId first for data-* attributes
        const divIdDotPath = 'ui.divId';
        const defaultRootDivId = OlzaUtil.dictGet(OlzaConfig.getDefaults(), divIdDotPath);
        const rootDivId = OlzaUtil.dictGet(userConfigFiltered, divIdDotPath, defaultRootDivId);
        const dataAttrsRaw = OlzaConfig.getDataAttributes(isModal, rootDivId, userConfigFiltered, clickedElement);
        const dataAttrs = OlzaUtil.sanitizeDict(dataAttrsRaw, defaultConfigDotPaths);

        // Use copy of defaults to overlay with merged user config and data-* attributes
        let combinedConfig = OlzaConfig.getDefaults();

        const codeFirst = OlzaUtil.dictGet(userConfigFiltered, 'codeFirst', false);
        const src = codeFirst ? userConfigFiltered : dataAttrs;
        const dest = codeFirst ? dataAttrs : userConfigFiltered;

        // We can ignore missing elements in source objects as we are merging
        // into widget defaults, which already contain all the elements.
        OlzaUtil.copyDictElements(src, dest, defaultConfigDotPaths, false);
        OlzaUtil.copyDictElements(dest, combinedConfig, defaultConfigDotPaths, false);

        return combinedConfig;
    },

    /**
     * @param {boolean} isModal
     * @param {string}  rootDivId
     * @param {object} userConfig
     * @param {object} clickedElement Object that was triggering this action. Used only data-* attributes.
     * @returns {{}}
     */
    getDataAttributes: function(isModal, rootDivId, userConfig, clickedElement) {
        // Let's get divId first for data-* attributes

        // Read data-* attributes from "element" that triggered the picker
        let dataAttrsRaw = {};
        if (isModal) {
            if (clickedElement !== null) {
                dataAttrsRaw = clickedElement.dataset || {};
            }
        } else {
            const rootDiv = $(`#${rootDivId}`);
            if (rootDiv.length === 1) {
                dataAttrsRaw = rootDiv.data();
            } else {
                // too many containers? log error and proceed.
                const msg = `Needs single "${rootDivId}" element to scan for data-* attributes, found: ${rootDiv.length}.`;
                OlzaLog.logError(msg);
            }
        }

        // map flat data-* attributes to config tree structure. It's not done automatically ATM, but via
        // mapper functions assigned to each supported data attribute.
        const attrMappers = {
            olzaApiUrl: (config, key, value) => OlzaUtil.dictSet(config, 'api.url', value),
            olzaApiAccessToken: (config, key, value) => OlzaUtil.dictSet(config, 'api.accessToken', value),
            olzaApiCountry: (config, key, value) => OlzaUtil.dictSet(config, 'api.country', value),
            olzaApiLatitude: (config, key, value) => OlzaUtil.dictSet(config, 'api.latitude', value),
            olzaApiLongitude: (config, key, value) => OlzaUtil.dictSet(config, 'api.longitude', value),
            olzaApiUseBrowserLocation: (config, key, value) => OlzaUtil.dictSet(config, 'api.useBrowserLocation', value),
            olzaApiSpeditions: (config, key, value) => {
                const speditionsList = [];
                (value.split(',')).forEach(function(spedCode) {
                    const code = spedCode.trim().toLowerCase();
                    if (code !== '' && speditionsList.indexOf(code) === -1) {
                        speditionsList.push(code);
                    }
                });
                OlzaUtil.dictSet(config, 'api.speditions', speditionsList);
            },

            olzaUiInitialZoom: (config, key, value) => OlzaUtil.dictSet(config, 'ui.initialZoom', value),
            olzaUiDetailsZoom: (config, key, value) => OlzaUtil.dictSet(config, 'ui.detailsZoom', value),
            olzaUiLanguage: (config, key, value) => OlzaUtil.dictSet(config, 'ui.language', value),

            olzaServiceTypeFiltersVisible: (config, key, value) => OlzaUtil.dictSet(config, 'ui.serviceTypeFilters.visible', value),
            olzaPaymentTypeFiltersVisible: (config, key, value) => OlzaUtil.dictSet(config, 'ui.paymentTypeFilters.visible', value),
        };

        const dataAttrsMapped = {};
        for (const attrKey of Object.keys(dataAttrsRaw)) {
            if (attrKey.substring(0, 4) !== 'olza') {
                continue;
            }
            if (attrKey in attrMappers) {
                // Call config mapper to set the attribute value in correct location.
                attrMappers[attrKey](dataAttrsMapped, attrKey, dataAttrsRaw[attrKey]);
            } else {
                OlzaLog.warn(`Unsupported attribute: ${attrKey}`);
            }
        }

        return dataAttrsMapped;
    },

    /** ***************************************************************************************** **/

};

/** ********************************************************************************************* **/

export default OlzaConfig
